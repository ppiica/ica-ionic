import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HelpPage } from './help';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from '../../components/components.module';



@NgModule({
  declarations: [
    HelpPage,
  ],
  imports: [
    IonicPageModule.forChild(HelpPage),
    TranslateModule.forChild(),
    ComponentsModule
  ],
})
export class HelpPageModule {}
