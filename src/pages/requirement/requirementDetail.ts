import {Item} from './../../models/evaluation';
import {Component} from '@angular/core';
import {Platform, NavParams, ViewController} from 'ionic-angular';
import {Camera, CameraOptions} from '@ionic-native/camera';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { ActionSheetController } from 'ionic-angular';


@Component({
  templateUrl: 'requirementDetail.html'
})
export class RequirementDetail {

  public evaluation: Item;
  public evaluationForm: FormGroup;
  private maxScore: number;
  private itemData: any;
  private picture: string;

  requirementToSave;

  constructor(
    public platform: Platform,
    public params: NavParams,
    public viewCtrl: ViewController,
    private camera: Camera,
    private formBuilder: FormBuilder,
    public actionSheetCtrl: ActionSheetController
  ) {
    this.evaluation = this.params.get('evaluation');
    this.maxScore = this.params.get('maxScore');
    this.itemData = this.params.get('itemData');
    this.requirementToSave = {};
    if (this.itemData && this.itemData.evidence) {
      this.picture = this.itemData.evidence;
    }
    this.initForm();
  }

  public save() {
    const data = {
      itemId: this.evaluation.id,
      score: new Number(this.evaluationForm.value.score),
      observation: this.evaluationForm.value.observation,
      evidence: this.picture
    };
    this.viewCtrl.dismiss(data);
  }

  public close() {
    this.viewCtrl.dismiss();
  }

  public selectSource(){
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Seleccionar fuente',
      buttons: [
        {
          text: 'Cámara',
          icon : 'camera',
          handler: () => {
            this.takePicture(true)
          }
        },{
          text: 'Galería',
          icon: 'images',
          handler: () => {
            this.takePicture(false)
          }
        },{
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();

  }

  public takePicture(isCamera: boolean) {
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: isCamera ?  this.camera.PictureSourceType.CAMERA : this.camera.PictureSourceType.PHOTOLIBRARY,
      targetWidth: 1280
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.picture = base64Image;
    }, (err) => {
      // Handle error
    });
  }

  private initForm() {
    this.evaluationForm = this.formBuilder.group({
      score: [(this.itemData ? this.itemData.score : undefined), Validators.compose([Validators.required, Validators.max(this.maxScore), Validators.min(0)]),],
      observation: [(this.itemData ? this.itemData.observation : undefined)]
    });
  }

}
