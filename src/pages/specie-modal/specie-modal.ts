import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Species } from "../../models/species";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ViewController } from "ionic-angular";

@IonicPage()
@Component({
  selector: 'page-specie-modal',
  templateUrl: 'specie-modal.html',
})
export class SpecieModalPage {

  public species: Species;
  public productiveActivity: any;
  public productiveActivityForm: FormGroup;
  public totalInventory: number = 0;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    private viewCtrl: ViewController
  ) {
    this.species = this.navParams.get('species');
    this.productiveActivity = this.navParams.get('productiveActivity');
    this.initForm();
  }

  ionViewDidLoad() {
  }

  public close() {
    this.viewCtrl.dismiss();
  }

  public save() {
    if (this.productiveActivityForm.valid) {
      const productiveActivity = this.productiveActivityForm.value;
      productiveActivity.specieId = this.species.id;
      if (productiveActivity.fullCycle) {
        productiveActivity.fullCycle = new Number(productiveActivity.fullCycle);
      } else {
        delete productiveActivity.fullCycle;
      }
      if (productiveActivity.breed) {
        productiveActivity.breed = new Number(productiveActivity.breed);
      } else {
        delete productiveActivity.breed;
      }
      if (productiveActivity.lift) {
        productiveActivity.lift = new Number(productiveActivity.lift);
      } else {
        delete productiveActivity.lift;
      }
      if (productiveActivity.fatten) {
        productiveActivity.fatten = new Number(productiveActivity.fatten);
      } else {
        delete productiveActivity.fatten;
      }
      this.viewCtrl.dismiss(productiveActivity);
    }
  }

  private initForm() {
    this.productiveActivityForm = this.formBuilder.group({
      fullCycle: [undefined, Validators.min(1)],
      breed: [undefined, Validators.min(1)],
      lift: [undefined, Validators.min(1)],
      fatten: [undefined, Validators.min(1)],
    });

    this.productiveActivityForm.valueChanges.subscribe((form) => {
      if (form) {
        this.getTotal(form);
      }
    });

    if (this.productiveActivity) {
      const valueForm = {
        fullCycle: this.productiveActivity.fullCycle,
        breed: this.productiveActivity.breed,
        lift: this.productiveActivity.lift,
        fatten: this.productiveActivity.fatten
      };
      this.productiveActivityForm.setValue(valueForm);
    }

  }

  private getTotal(form) {
    this.totalInventory = 0;
    if (form.fullCycle > 0) {
      this.totalInventory += form.fullCycle;
    }
    if (form.breed > 0) {
      this.totalInventory += form.breed;
    }
    if (form.lift > 0) {
      this.totalInventory += form.lift;
    }
    if (form.fatten > 0) {
      this.totalInventory += form.fatten;
    }
  }

}
