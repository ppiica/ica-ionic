import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpecieModalPage } from './specie-modal';
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  declarations: [
    SpecieModalPage,
  ],
  imports: [
    IonicPageModule.forChild(SpecieModalPage),
    TranslateModule.forChild()
  ],
})
export class SpecieModalPageModule {}
