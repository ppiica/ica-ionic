import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';


@IonicPage()
@Component({
  selector: 'page-signature',
  templateUrl: 'signature.html',
})
export class SignaturePage {

  public initDraw = false;
  public isDrawing = false;
  public data: { signature: string };
  public title: string;

  @ViewChild(SignaturePad) signaturePad: SignaturePad;
  public signaturePadOptions = {
    'minWidth': 1,
    'canvasWidth': 340,
    'canvasHeight': 400,
    'backgroundColor': '#fff',
    'penColor': '#000'
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController
  ) {
    this.title = this.navParams.get('title');
  }

  ionViewDidEnter() {
    var element = document.getElementById("canvas");
    element.classList.add("signature-pad");
    this.signaturePad.resizeCanvas();
    this.signaturePad.clear();
  }

  ionViewDidLeave() {
  }


  public drawComplete() {
    this.isDrawing = false;
  }

  public drawStart() {
    this.initDraw = true;
    this.isDrawing = true;
  }

  public savePad() {
    const signature = this.signaturePad.toDataURL();
    this.signaturePad.clear();
    this.close(signature);
  }

  public clearPad() {
    this.initDraw = false;
    this.signaturePad.clear();
  }

  public close(signature?: string) {
    this.viewCtrl.dismiss({
      signature
    });
  }

}
