import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VisitsListPage } from './visits-list';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    VisitsListPage,
  ],
  imports: [
    IonicPageModule.forChild(VisitsListPage),
    TranslateModule.forChild(),
    ComponentsModule
  ],
})
export class VisitsListPageModule {}
