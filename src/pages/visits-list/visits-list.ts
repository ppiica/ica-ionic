import { AlertServiceProvider } from './../../providers/alert-service/alert-service';
import { SpeciesProvider } from './../../providers/species/species';
import {EvaluationsServiceProvider} from './../../providers/evaluations-service/evaluations-service';
import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ModalController, Loading} from 'ionic-angular';
import {Observable} from 'rxjs/Observable';
import {VisitService} from './../../providers/visit-service/visit-service';
import {Visit} from './../../models/visitModel';
import {LoadingServiceProvider} from "../../providers/loading-service/loading-service";
import * as moment from "moment";

@IonicPage()
@Component({
  selector: 'page-visits-list',
  templateUrl: 'visits-list.html',
})
export class VisitsListPage {

  public loading: Loading;
  public visits: Observable<Visit[]>;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private modalCtrl: ModalController,
    private visitService: VisitService,
    private evaluationService: EvaluationsServiceProvider,
    private loadingService: LoadingServiceProvider,
    private speciesService: SpeciesProvider,
    private alertService: AlertServiceProvider
  ) {
    // this.observeLoading();
  }

  ionViewCanEnter() {
    this.loadInitData();
  }

  public openVisit(visit: Visit) {
    const vistDate = moment(visit.date);
    const currentDate = moment();
    if (currentDate.diff(vistDate, 'days') <= 0) {
      const modal = this.modalCtrl.create('CheckListPage', { visit });
      modal.present();
      modal.onDidDismiss(() => {
        this.loadInitData()
      })
    } else {
      this.alertService.showAlert('WARNING', 'VISIT_EXPIRE');
    }
  }

  private loadInitData() {
    this.visits = this.visitService.getVisits();
    this.evaluationService.getCategories();
    this.speciesService.getSpecies();
  }

  private observeLoading() {
    this.visitService.loadingVisits$.subscribe(async (value) => {
      if (value) {
        this.loading = await this.loadingService.getLoading('LOADING');
        this.loading.present();
      } else {
        if (this.loading) {
          this.loading.dismiss();
        }
      }
    });
  }

}
