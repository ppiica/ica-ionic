import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RequirementModalPage } from './requirement-modal';

@NgModule({
  declarations: [
    RequirementModalPage,
  ],
  imports: [
    IonicPageModule.forChild(RequirementModalPage),
  ],
})
export class RequirementModalPageModule {}
