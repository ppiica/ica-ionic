import {LoadingServiceProvider} from './../../providers/loading-service/loading-service';
import {ToastServiceProvider} from './../../providers/toast-service/toast-service';
import {Component} from '@angular/core';
import {IonicPage, NavController, Loading} from 'ionic-angular';
import {FormBuilder, FormGroup, Validator, Validators} from '@angular/forms';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public loginForm: FormGroup;

  constructor(
    public navCtrl: NavController,
    private formBuilder: FormBuilder,
    private authService: AuthServiceProvider,
    private toastService: ToastServiceProvider,
    private loadingService: LoadingServiceProvider
  ) {
  }

  ionViewCanEnter() {
    this.initForm();
  }

  public async recoverPassword() {
    if (this.loginForm.get('email').valid) {
      let loading: Loading;
      try {
        loading = await this.loadingService.getLoading('LOADING');
        loading.present();
        await this.authService.recoverPassword(this.loginForm.value.email);
        loading.dismiss();
        this.toastService.showToast('EMAIL_TO_RECOVER_PASSWORD');
      } catch (error) {
        console.error(error);
        if (loading) {
          loading.dismiss();
        }
        this.toastService.showToast('CANT_NO_SEND_EMAIL_TO_RECOVER_PASSWORD');
      }
    }
  }

  public async submit() {
    try {
      const loading = await this.loadingService.getLoading('LOADING');
      loading.present();
      try {
        if (this.loginForm.valid) {
          await  this.authService.login(this.loginForm.value.email, this.loginForm.value.password);
          loading.dismiss();
          this.goToCheckList();
        }
      } catch (error) {
        loading.dismiss();
      }
    } catch (e) {
      console.log(e);
    }
  }

  private initForm() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.email],
      password: ['', Validators.required]
    });
  }

  /**
   * goToCheckList
   */
  private goToCheckList() {
    this.navCtrl.setRoot('VisitsListPage');
  }

}
