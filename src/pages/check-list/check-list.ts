import { LoadingServiceProvider } from './../../providers/loading-service/loading-service';
import { Visit } from './../../models/visitModel';
import { Category, Item } from './../../models/evaluation';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EvaluationsServiceProvider } from './../../providers/evaluations-service/evaluations-service';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides, ModalController, ViewController, Loading, Content, AlertController } from 'ionic-angular';
import { RequirementDetail } from '../../pages/requirement/requirementDetail';
import { VisitDataProvider } from "../../providers/visit-data/visit-data";
import * as _ from 'lodash';
import { VisitService } from "../../providers/visit-service/visit-service";
import { SpeciesProvider } from "../../providers/species/species";
import { Species } from "../../models/species";
import moment from "moment";

@IonicPage()
@Component({
  selector: 'page-check-list',
  templateUrl: 'check-list.html',
})
export class CheckListPage {

  public categories: Category[];
  public species: Species[];
  public visitForm: FormGroup;
  public visit: Visit;
  private visitData: any;
  public visitedData: any;
  public visitedFormIsValid: boolean;

  @ViewChild('formSlides')
  public formSlides: Slides;

  @ViewChild(Content)
  public content: Content;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController,
    public evaluationService: EvaluationsServiceProvider,
    private formBuilder: FormBuilder,
    private visitDataService: VisitDataProvider,
    private visitService: VisitService,
    private speciesService: SpeciesProvider,
    private loadingService: LoadingServiceProvider,
    private alertCtrl: AlertController
  ) {
    this.visit = this.navParams.get('visit');
    this.visitData = {
      id: this.visit.id,
      qualifications: [],
      productiveActivities: []
    }
    this.init();
  }

  ionViewDidLoad() {
    this.formSlides.onlyExternal = true;
  }

  public scrollToTop() {
    this.content.scrollToTop();
  }

  public openEvaluation(evaluation: Item, maxScore: number) {
    let itemData;
    if (this.visitData.qualifications && evaluation && evaluation.id) {
      itemData = _.find(this.visitData.qualifications, { itemId: evaluation.id });
    }
    let modal = this.modalCtrl.create(RequirementDetail, { evaluation, maxScore, itemData });
    modal.present();
    modal.onDidDismiss(data => {
      if (data) {
        this.saveItemValue(data);
      }
    });
  }

  public openSpecies(species: Species) {
    let productiveActivity;
    if (this.visitData.productiveActivities && species && species.id) {
      productiveActivity = _.find(this.visitData.productiveActivities, { specieId: species.id });
    }
    const modal = this.modalCtrl.create('SpecieModalPage', { species, productiveActivity });
    modal.present()
      .catch(console.error);
    modal.onDidDismiss((productiveActivity) => {
      if (productiveActivity) {
        this.saveProductiveActivity(productiveActivity);
      }
    });
  }

  /**
   * next
   */
  public next() {
    console.log(this);
    console.log(this.formSlides.getActiveIndex());
    this.formSlides.slideNext();
    this.scrollToTop();
  }

  /**
   * back
   */
  public back() {
    console.log(this);
    this.formSlides.slidePrev();
    this.scrollToTop();
  }

  public close() {
    this.viewCtrl.dismiss();
  }

  public visitedChange(visited) {
    if (visited) {
      this.visitData.personWhoAttendsId = new Number(visited.personWhoAttendsId);
      this.visitData.personWhoAttendsName = visited.personWhoAttendsName;
      this.visitData.personWhoAttendsPhone = new Number(visited.personWhoAttendsPhone);
      this.saveVisitData();
    }
  }

  public visitedFormValidChange(valid) {
    this.visitedFormIsValid = valid;
  }

  public askToFinish() {
    if (this.canFinish()) {
      let alert = this.alertCtrl.create({
        title: '¿Seguro?',
        message: '¿Desea finalizar la visita? Recuerde que luego de finalizar no puede modificar la visita',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            handler: () => {
            }
          },
          {
            text: 'Finalizar',
            handler: () => {
              this.finish();
            }
          }
        ]
      });
      alert.present();
    }

  }

  private canFinish() {
    const youCanFinishProductiveActivity = this.visitData && this.visitData.productiveActivities && this.visitData.productiveActivities.length;

    const youCanFinishQualifications = this.visitData && this.visitData.qualifications && this.visitData.qualifications.length;

    if (!youCanFinishProductiveActivity) {
      let alert = this.alertCtrl.create({
        title: '¡Advertencia!',
        message: 'No puedes finalizar la visita sin haber ingresado al menos un inventario',
      });
      alert.present();
    }

    if (!youCanFinishQualifications) {
      let alert = this.alertCtrl.create({
        title: '¡Advertencia!',
        message: 'No puedes finalizar la visita sin haber ingresado al menos una calificación',
      });
      alert.present();
    }

    return youCanFinishProductiveActivity && youCanFinishQualifications;
  }

  private async finish() {
    const loading = await this.loadingService.getLoading("LOADING");
    loading.present();
    const visitToFinish: Visit = JSON.parse(JSON.stringify(this.visit));

    if (this.visitData.qualifications) {
      visitToFinish.qualifications = [...this.visitData.qualifications];
      for (let i = 1; i < visitToFinish.qualifications.length; i++) {
        let qualification = visitToFinish.qualifications[i];
        if (!qualification) {
          let categoryMaxScore;

          for (let j = 0; j < this.categories.length; j++) {
            const category = this.categories[j];
            for (let k = 0; k < category.items.length; k++) {
              const item = category.items[k];
              if (item.id == i) {
                categoryMaxScore = category.maxScore;
              }
            }
          }

          qualification = {
            itemId: i,
            score: categoryMaxScore
          }
        }
      }
      visitToFinish.qualifications = _.filter(visitToFinish.qualifications, Boolean);
    }

    if (this.visitData.productiveActivities) {
      visitToFinish.productiveActivities = [...this.visitData.productiveActivities];
      visitToFinish.productiveActivities = _.filter(visitToFinish.productiveActivities, Boolean);
    }

    visitToFinish.personWhoAttendsId = this.visitData.personWhoAttendsId;
    visitToFinish.personWhoAttendsName = this.visitData.personWhoAttendsName;
    visitToFinish.personWhoAttendsPhone = this.visitData.personWhoAttendsPhone;

    visitToFinish.ownerFarmSign = this.visitData.ownerFarmSign;
    visitToFinish.inspectorSign = this.visitData.inspectorSign;

    visitToFinish.observations = this.visitData.observations;
    visitToFinish.gradualCompliancePlan = this.visitData.gradualCompliancePlan;

    delete visitToFinish.farm;
    delete visitToFinish.status;
    delete visitToFinish.technical;
    delete visitToFinish.nextVisitDate;
    if (visitToFinish.observations === undefined || visitToFinish.observations === null) {
      delete visitToFinish.observations;
    }
    if (visitToFinish.gradualCompliancePlan === undefined || visitToFinish.gradualCompliancePlan === null) {
      delete visitToFinish.gradualCompliancePlan;
    }

    console.log(JSON.stringify(visitToFinish));
    try {
      await this.visitService.finsihVisit(visitToFinish.id, visitToFinish);
      loading.dismiss();
      this.close();
    } catch (e) {
      loading.dismiss();
      console.log(e);
    }
  }

  public validateSlide() {
    switch (this.formSlides.getActiveIndex()) {
      // form visited
      case 3:

        break;
    }
  }

  public itemColor(maxScore, score): string {
    const sixtyPercent = maxScore * 0.6;
    const eightyPercent = maxScore * 0.8;

    if (score < sixtyPercent) {
      return 'danger';
    } else if (score >= sixtyPercent && score < eightyPercent) {
      return 'secondary';
    } else {
      return 'primary';
    }

  }

  private async initData() {
    try {
      this.categories = await this.evaluationService.getCategories();
      this.species = await this.speciesService.getSpecies();
    } catch (error) {
      console.error(error);
    }
  }

  private initForm() {
    this.visitForm = this.formBuilder.group({
      inspectorSign: [undefined, Validators.required],
      ownerFarmSign: [undefined, Validators.required],
      nextVisitDate: [undefined],
      observations: [undefined],
      gradualCompliancePlan: [undefined],
    });

    this.visitForm.valueChanges.subscribe((value) => {
      if (value) {
        if (value.inspectorSign) {
          this.visitData.inspectorSign = value.inspectorSign
        }
        if (value.ownerFarmSign) {
          this.visitData.ownerFarmSign = value.ownerFarmSign
        }
        if (value.nextVisitDate) {
          this.visitData.nextVisitDate = value.nextVisitDate
        }
        if (value.observations) {
          this.visitData.observations = value.observations
        }
        if (value.gradualCompliancePlan) {
          this.visitData.gradualCompliancePlan = value.gradualCompliancePlan;
        }
      }

      this.saveVisitData();
    });
  }

  public getCurrentDate() {
    return moment().format();
  }

  private saveItemValue(itemValue) {
    if (itemValue) {
      if (!this.visitData.qualifications) {
        this.visitData.qualifications = [];
      }
      this.visitData.qualifications[itemValue.itemId] = itemValue;
      console.log(this.visitData);
      this.saveVisitData();
    }
  }

  private saveProductiveActivity(productiveActivity) {
    if (productiveActivity) {
      if (!this.visitData.productiveActivities) {
        this.visitData.productiveActivities = [];
      }
      this.visitData.productiveActivities[productiveActivity.specieId] = productiveActivity;
      console.log(this.visitData);
      this.saveVisitData();
    }
  }

  private async getVisitData() {
    try {
      const visitData = await this.visitDataService.getVisitData(this.visit.id)
      if (visitData) {
        this.visitData = visitData;
        const visitFormValue = {
          personWhoAttendsId: this.visitData.personWhoAttendsId,
          personWhoAttendsName: this.visitData.personWhoAttendsName,
          personWhoAttendsPhone: this.visitData.personWhoAttendsPhone,
        };
        this.visitedData = visitFormValue;
        if (!this.visitData.qualifications) {
          this.visitData.qualifications = [];
        }
        const formValue: any = {
          inspectorSign: '',
          ownerFarmSign: '',
          observations: '',
          gradualCompliancePlan: '',
          nextVisitDate: null
        };
        if (this.visitData.inspectorSign) {
          formValue.inspectorSign = this.visitData.inspectorSign;
        }
        if (this.visitData.ownerFarmSign) {
          formValue.ownerFarmSign = this.visitData.ownerFarmSign;
        }
        if (this.visitData.observations) {
          formValue.observations = this.visitData.observations;
        }
        if (this.visitData.gradualCompliancePlan) {
          formValue.gradualCompliancePlan = this.visitData.gradualCompliancePlan;
        }
        if (this.visitData.nextVisitDate) {
          formValue.nextVisitDate = this.visitData.nextVisitDate;
        }
        if (!this.visitData.productiveActivities) {
          this.visitData.productiveActivities = [];
        }
        this.visitForm.setValue(formValue);
      }
    } catch (e) {
      console.error(e);
    }
  }

  private saveVisitData() {
    console.log(this.visitData);
    this.visitDataService.saveVisitData(this.visitData);
  }

  private async init() {
    try {
      const loading: Loading = await this.loadingService.getLoading('LOADING');
      loading.present();
      await this.initForm();
      await this.initData();
      await this.getVisitData();
      loading.dismiss();
    } catch (error) {
      console.log(error)
    }
  }

}
