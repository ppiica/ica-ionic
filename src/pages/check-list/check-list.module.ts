import { PipesModule } from './../../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CheckListPage } from './check-list';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    CheckListPage,
  ],
  imports: [
    IonicPageModule.forChild(CheckListPage),
    TranslateModule.forChild(),
    ComponentsModule,
    PipesModule,
  ],
})
export class CheckListPageModule {}
