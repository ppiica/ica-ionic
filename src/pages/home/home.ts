import { Component } from '@angular/core';
import { NavController, IonicPage, MenuController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, menuCtrl: MenuController) {

  }

  /**
   * goToForm
   */
  public goToForm() {
    this.navCtrl.push('CheckListPage');
  }

  public goToVisits() {
    this.navCtrl.push('VisitsListPage');
  }

  public goToAbout(){
    this.navCtrl.push('AboutPage');
  }

  public goToHelp(){
    this.navCtrl.push('HelpPage');
  }
}
