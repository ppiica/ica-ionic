import { AuthServiceProvider } from './../providers/auth-service/auth-service';
import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpSentEvent, HttpHeaderResponse, HttpProgressEvent, HttpResponse, HttpUserEvent } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/mergeMap';
import { Token } from '@angular/compiler';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  private authService: AuthServiceProvider;

  constructor(
    private injector: Injector
  ) {

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {

    this.authService = this.injector.get(AuthServiceProvider);

    return this.authService.getToken()
      .mergeMap(token => {
        console.log(token);

        if (token) {
          req = req.clone({
            setHeaders: {
              token: token,
            }
          });
        }

        return next.handle(req);
      })

  }

}
