import { Category } from './../../models/evaluation';
import { Environments } from './../../environments/environments';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { DataBaseProvider } from '../data-base/data-base';
import { NetworkServiceProvider } from '../network-service/network-service';
import { SQLiteObject } from '@ionic-native/sqlite';
import { Queries } from '../../environments/sqlQueries';

@Injectable()
export class EvaluationsServiceProvider {

  private db: SQLiteObject;
  private online: boolean;

  constructor(
    public http: HttpClient,
    public dataBaseService: DataBaseProvider,
    private networkService: NetworkServiceProvider
  ) {
    this.getDB();
    this.networkService.networkBhs.subscribe((status) => this.online = status);
  }

  public getCategories(): Promise<Category[]> {
    return new Promise((resolve, reject) => {
      if (this.online) {
        this.getCategoriesBackend()
          .then((categories) => {
            resolve(categories);
            this.insertCategories(categories);
          })
          .catch((error) => {
            console.log(error);
            this.getCategoriesFromLocal()
              .then(resolve);
          });
      } else {
        this.getCategoriesFromLocal()
          .then(resolve);
      }
    });
  }

  private getCategoriesBackend(): Promise<Category[]> {
    return this.http.get<Category[]>(Environments.ENDPOINTS.CATEGORIES).toPromise();
  }

  private async getCategoriesFromLocal(): Promise<any> {
    try {
      await this.getDB();
      const rs = await this.db.executeSql(Queries.CATEGORY_QUERIES.SELECT_CATEGORIES, []);
      if (rs && rs.rows.length) {
        const categories: Category[] = [];
        for (let i = 0; i < rs.rows.length; i++) {
          const category = JSON.parse(rs.rows.item(i).json);
          categories.push(category);
        }
        return categories;
      } else {
        return [];
      }
    } catch (err) {
      return Promise.reject(err);
    }
  }

  private insertCategories(categories: Category[]) {
    if (categories && categories.length) {
      for (let i = 0; i < categories.length; i++) {
        const category = categories[i];
        this.insertCategory(category);
      }
    }
  }

  private async insertCategory(category: Category) {
    if (category) {
      try {
        await this.getDB();
        const queryParams = [category.id, JSON.stringify(category)];
        await this.db.executeSql(Queries.CATEGORY_QUERIES.INSERT_CATEGORY, queryParams)
      } catch (e) {
        console.error(e);
      }
    }
  }

  private async getDB() {
    this.db = await this.dataBaseService.getDataBase();
  }

}
