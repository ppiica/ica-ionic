import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';

@Injectable()
export class ToastServiceProvider {

  constructor(
    private toastCtrl: ToastController,
    private translate: TranslateService
  ) {
  }

  public showToast(message: string) {
    this.translate.get(message).subscribe(messageTranslated => {
      this.toastCtrl.create({
        message: messageTranslated,
        duration: 2500
      }).present();
    });
  }

}
