import { NetworkServiceProvider } from './../network-service/network-service';
import { catchError, tap, } from 'rxjs/operators';
import { SQLiteObject } from '@ionic-native/sqlite';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Visit } from './../../models/visitModel';
import { Environments } from '../../environments/environments';
import { Queries } from '../../environments/sqlQueries';
import { DataBaseProvider } from './../data-base/data-base';
import { fromPromise } from 'rxjs/observable/fromPromise';
import { LoadingServiceProvider } from "../loading-service/loading-service";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { VisitDataProvider } from "../visit-data/visit-data";

@Injectable()
export class VisitService {

  private db: SQLiteObject;
  private online: boolean;
  public loadingVisits$: BehaviorSubject<boolean>;

  constructor(
    public http: HttpClient,
    public dataBaseService: DataBaseProvider,
    private networkService: NetworkServiceProvider,
    private loadingService: LoadingServiceProvider,
    private visitDataService: VisitDataProvider
  ) {
    this.loadingVisits$ = new BehaviorSubject<boolean>(false);
    this.getDB();
    this.networkService.networkBhs.subscribe((status) => {
      this.online = status;
      if (this.online) {
        this.syncServices();
        this.getVisits();
      }
    });
  }

  public getVisits(): Observable<Visit[]> {
    this.loadingVisits$.next(true);
    if (this.online) {
      return this.getVisitsOnline();
    } else {
      return fromPromise<Visit[]>(this.getVisitsOffline());
    }
  }

  public async updateVisit(visit: Visit): Promise<boolean> {
    try {
      await this.getDB();
      if (visit && visit.id) {
        const queryParams = [JSON.stringify(visit), visit.status, visit.id];
        const rs = await this.db.executeSql(Queries.VISITS_QUERIES.UPDATE_VISIT, queryParams);
        if (rs && rs.rows.length) {
          return true;
        } else {
          return false;
        }
      }
    } catch (error) {
      return Promise.reject(error);
    }
  }

  public async finsihVisit(id: number, data) {
    if (this.online) {
      return this.finsihVisitOnline(id, data);
    } else {
      return this.finsihVisitOffline(id, data);
    }
  }

  private getVisitsOnline(): Observable<Visit[]> {
    let loggedUser: any = localStorage.getItem(Environments.STORAGE_KEYS.LOGGED_USER);
    loggedUser = JSON.parse(loggedUser);
    return this.http.get<Visit[]>(Environments.ENDPOINTS.VISITS.replace('{{id}}', loggedUser.id))
      .pipe(
        tap((visits) => {
          this.insertVisits(visits);
          this.loadingVisits$.next(false);
        }),
        catchError((err, caught) => {
          console.log(err);
          return this.getVisitsOffline();
        })
      );
  }

  private async getVisitsOffline(): Promise<Visit[]> {
    try {
      await this.getDB();
      const rs = await this.db.executeSql(Queries.VISITS_QUERIES.SELECT_ALL_VISITS, []);
      if (rs && rs.rows.length) {
        const visits: Visit[] = [];
        for (let i = 0; i < rs.rows.length; i++) {
          const visit = JSON.parse(rs.rows.item(i).json);
          visits.push(visit);
        }
        this.loadingVisits$.next(false);
        return visits;
      } else {
        this.loadingVisits$.next(false);
        return [];
      }
    } catch (err) {
      this.loadingVisits$.next(false);

      return Promise.reject(err);
    }
  }

  private insertVisits(visits: Visit[]): void {
    if (visits && visits.length) {
      for (let i = 0; i < visits.length; i++) {
        const visit = visits[i];
        this.insertVisit(visit);
      }
    }
  }

  private async insertVisit(visit: Visit) {
    try {
      await this.getDB();
      if (visit) {
        const queryParams = [visit.id, JSON.stringify(visit), visit.status, 0];
        this.db.executeSql(Queries.VISITS_QUERIES.INSERT_VISIT, queryParams)
          .catch(console.error);
      }
    } catch (e) {
      console.log(e);
    }
  }

  private async finsihVisitOnline(id: number, data) {
    await this.http.post<Visit[]>(
      Environments.ENDPOINTS.FINISH_VISITS.replace('{{id}}', `${id}`),
      data
    )
      .pipe(
        tap(() => {
          this.deleteService(id);
        }),
        catchError(err => {
          return this.finsihVisitOffline(id, data);
        })
      )
      .toPromise();
  }

  private async finsihVisitOffline(id: number, data) {
    await this.getDB();
    const queryParams = [1, 1, id];
    await this.db.executeSql(Queries.VISITS_QUERIES.UPDATE_VISIT, queryParams);
    await this.visitDataService.saveVisitData(data);
  }

  private async syncServices() {
    await this.getDB();
    const rs = await this.db.executeSql(Queries.VISITS_QUERIES.SELECT_ALL_VISITS_TO_SYNC, []);
    const visitsToSync = [];
    if (rs && rs.rows.length) {
      for (let i = 0; i < rs.rows.length; i++) {
        const visit: Visit = JSON.parse(rs.rows.item(i).json);
        const visitData = await this.visitDataService.getVisitData(visit.id);
        await this.finsihVisitOnline(visit.id, visitData);
      }
    }
  }

  private async deleteService(id) {
    console.log("remove visit with id:", id);
    try {
      await this.getDB();
      await this.db.executeSql(Queries.VISITS_QUERIES.DELETE_VISIT, [id]);
      await this.db.executeSql(Queries.VISIT_DATA_QUERY.DELETE_VISIT_DATA, [id]);
    } catch (error) {
      console.error(error);
    }

  }

  private async getDB() {
    if (!this.db) {
      this.db = await this.dataBaseService.getDataBase();
    }
  }

}
