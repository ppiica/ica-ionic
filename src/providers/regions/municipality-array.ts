export const municipalitiesArray = [{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb6b"
	},
	"id": 76892,
	"nombre": "YUMBO",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb70"
	},
	"id": 76233,
	"nombre": "DAGUA",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb7a"
	},
	"id": 76616,
	"nombre": "RIOFRÍO",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb84"
	},
	"id": 73671,
	"nombre": "SALDAÑA",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb9d"
	},
	"id": 73268,
	"nombre": "ESPINAL",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb93"
	},
	"id": 73449,
	"nombre": "MELGAR",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb89"
	},
	"id": 73226,
	"nombre": "CUNDAY",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebac"
	},
	"id": 52838,
	"nombre": "TÚQUERRES",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb75"
	},
	"id": 76126,
	"nombre": "CALIMA",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eba2"
	},
	"id": 52233,
	"nombre": "CUMBITARA",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebb6"
	},
	"id": 73168,
	"nombre": "CHAPARRAL",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebbb"
	},
	"id": 50711,
	"nombre": "VISTAHERMOSA",
	"idDepartamento": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb8e"
	},
	"id": 73217,
	"nombre": "COYAIMA",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebc5"
	},
	"id": 52381,
	"nombre": "LA FLORIDA",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebcf"
	},
	"id": 50350,
	"nombre": "LA MACARENA",
	"idDepartamento": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eba7"
	},
	"id": 52585,
	"nombre": "PUPIALES",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebc0"
	},
	"id": 63302,
	"nombre": "GÉNOVA",
	"idDepartamento": 63,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebd4"
	},
	"id": 50577,
	"nombre": "PUERTO LLERAS",
	"idDepartamento": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebde"
	},
	"id": 50330,
	"nombre": "MESETAS",
	"idDepartamento": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebd9"
	},
	"id": 50318,
	"nombre": "GUAMAL",
	"idDepartamento": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebe8"
	},
	"id": 50001,
	"nombre": "VILLAVICENCIO",
	"idDepartamento": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebf2"
	},
	"id": 19142,
	"nombre": "CALOTO",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebed"
	},
	"id": 19075,
	"nombre": "BALBOA",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebf7"
	},
	"id": 19212,
	"nombre": "CORINTO",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec01"
	},
	"id": 41206,
	"nombre": "COLOMBIA",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec06"
	},
	"id": 41001,
	"nombre": "NEIVA",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec0b"
	},
	"id": 19743,
	"nombre": "SILVIA",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec10"
	},
	"id": 19532,
	"nombre": "PATÍA (EL BORDO)",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec1a"
	},
	"id": 19355,
	"nombre": "INZÁ",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec24"
	},
	"id": 41518,
	"nombre": "PAICOL",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec1f"
	},
	"id": 19001,
	"nombre": "POPAYÁN",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec29"
	},
	"id": 41357,
	"nombre": "ÍQUIRA",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec33"
	},
	"id": 19824,
	"nombre": "TOTORÓ",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec3d"
	},
	"id": 41396,
	"nombre": "LA PLATA",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec38"
	},
	"id": 41799,
	"nombre": "TELLO",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec42"
	},
	"id": 41676,
	"nombre": "SANTA MARÍA",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec4c"
	},
	"id": 41668,
	"nombre": "SAN AGUSTÍN",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec56"
	},
	"id": 18094,
	"nombre": "BELÉN DE LOS ANDAQUÍES",
	"idDepartamento": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb7f"
	},
	"id": 19473,
	"nombre": "MORALES",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec65"
	},
	"id": 73067,
	"nombre": "ATACO",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb98"
	},
	"id": 73616,
	"nombre": "RIOBLANCO",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec6f"
	},
	"id": 52411,
	"nombre": "LINARES",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebb1"
	},
	"id": 52693,
	"nombre": "SAN PABLO",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebca"
	},
	"id": 52254,
	"nombre": "EL PEÑOL",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec7e"
	},
	"id": 86568,
	"nombre": "PUERTO ASIS",
	"idDepartamento": 86,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebe3"
	},
	"id": 50006,
	"nombre": "ACACÍAS",
	"idDepartamento": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec88"
	},
	"id": 52540,
	"nombre": "POLICARPA",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebfc"
	},
	"id": 19022,
	"nombre": "ALMAGUER",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec97"
	},
	"id": 18001,
	"nombre": "FLORENCIA",
	"idDepartamento": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eca1"
	},
	"id": 18592,
	"nombre": "PUERTO RICO",
	"idDepartamento": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecb0"
	},
	"id": 86757,
	"nombre": "SAN MIGUEL",
	"idDepartamento": 86,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec6a"
	},
	"id": 18150,
	"nombre": "CARTAGENA DEL CHAIRÁ",
	"idDepartamento": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecba"
	},
	"id": 52323,
	"nombre": "GUALMATAN",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec47"
	},
	"id": 41551,
	"nombre": "PITALITO",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec60"
	},
	"id": 41006,
	"nombre": "ACEVEDO",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecc9"
	},
	"id": 19785,
	"nombre": "SUCRE",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec79"
	},
	"id": 76275,
	"nombre": "FLORIDA",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec83"
	},
	"id": 86569,
	"nombre": "PUERTO CAICEDO",
	"idDepartamento": 86,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecd3"
	},
	"id": 52250,
	"nombre": "EL CHARCO",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec92"
	},
	"id": 18753,
	"nombre": "SAN VICENTE DEL CAGUÁN",
	"idDepartamento": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ece2"
	},
	"id": 52079,
	"nombre": "BARBACOAS",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecab"
	},
	"id": 86885,
	"nombre": "VILLAGARZON",
	"idDepartamento": 86,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec9c"
	},
	"id": 18756,
	"nombre": "SOLANO",
	"idDepartamento": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecec"
	},
	"id": 68615,
	"nombre": "RIONEGRO",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecc4"
	},
	"id": 52612,
	"nombre": "RICAURTE",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec5b"
	},
	"id": 41801,
	"nombre": "TERUEL",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecfb"
	},
	"id": 94663,
	"nombre": "MAPIRIPANA",
	"idDepartamento": 94,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecb5"
	},
	"id": 52687,
	"nombre": "SAN LORENZO",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed14"
	},
	"id": 5107,
	"nombre": "BRICEÑO",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eca6"
	},
	"id": 86573,
	"nombre": "LEGUIZAMO",
	"idDepartamento": 86,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecce"
	},
	"id": 50573,
	"nombre": "PUERTO LOPEZ",
	"idDepartamento": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed2d"
	},
	"id": 76041,
	"nombre": "ANSERMANUEVO",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecd8"
	},
	"id": 50313,
	"nombre": "GRANADA",
	"idDepartamento": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ece7"
	},
	"id": 52621,
	"nombre": "ROBERTO PAYAN",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed37"
	},
	"id": 54800,
	"nombre": "TEORAMA",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed00"
	},
	"id": 99773,
	"nombre": "CUMARIBO",
	"idDepartamento": 99,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed46"
	},
	"id": 73283,
	"nombre": "FRESNO",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed0a"
	},
	"id": 76036,
	"nombre": "ANDALUCIA",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed23"
	},
	"id": 5129,
	"nombre": "CALDAS",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed50"
	},
	"id": 73686,
	"nombre": "SANTA ISABEL",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecdd"
	},
	"id": 76109,
	"nombre": "BUENAVENTURA",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed5f"
	},
	"id": 5890,
	"nombre": "YOLOMBÓ",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecf6"
	},
	"id": 50568,
	"nombre": "PUERTO GAITÁN",
	"idDepartamento": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed69"
	},
	"id": 73001,
	"nombre": "IBAGUÉ",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed78"
	},
	"id": 5895,
	"nombre": "ZARAGOZA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed28"
	},
	"id": 76845,
	"nombre": "ULLOA",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed41"
	},
	"id": 5197,
	"nombre": "COCORNÁ",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed32"
	},
	"id": 70215,
	"nombre": "COROZAL",
	"idDepartamento": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed91"
	},
	"id": 23001,
	"nombre": "MONTERÍA",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed5a"
	},
	"id": 5887,
	"nombre": "YARUMAL",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed73"
	},
	"id": 20614,
	"nombre": "RIO DE ORO",
	"idDepartamento": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed9b"
	},
	"id": 68101,
	"nombre": "BOLÍVAR",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed4b"
	},
	"id": 5234,
	"nombre": "DABEIBA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed8c"
	},
	"id": 54128,
	"nombre": "CÁCHIRA",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edaa"
	},
	"id": 54206,
	"nombre": "CONVENCIÓN",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edb4"
	},
	"id": 68397,
	"nombre": "LA PAZ",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed64"
	},
	"id": 73200,
	"nombre": "COELLO",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edbe"
	},
	"id": 68377,
	"nombre": "LA BELLEZA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edc3"
	},
	"id": 68271,
	"nombre": "FLORIÁN",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed3c"
	},
	"id": 73411,
	"nombre": "LÍBANO",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edcd"
	},
	"id": 66456,
	"nombre": "MISTRATÓ",
	"idDepartamento": 66,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed7d"
	},
	"id": 5893,
	"nombre": "YONDÓ (CASABE)",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed55"
	},
	"id": 5212,
	"nombre": "COPACABANA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed6e"
	},
	"id": 20400,
	"nombre": "LA JAGUA DE IBIRICO",
	"idDepartamento": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed96"
	},
	"id": 23182,
	"nombre": "CHINÚ",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed87"
	},
	"id": 23660,
	"nombre": "SAHAGÚN",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ede6"
	},
	"id": 25386,
	"nombre": "LA MESA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edaf"
	},
	"id": 68464,
	"nombre": "MOGOTES",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edc8"
	},
	"id": 66318,
	"nombre": "GUÁTICA",
	"idDepartamento": 66,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edff"
	},
	"id": 25772,
	"nombre": "SUESCA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edb9"
	},
	"id": 68264,
	"nombre": "ENCINO",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ede1"
	},
	"id": 25402,
	"nombre": "LA VEGA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edd2"
	},
	"id": 63548,
	"nombre": "PIJAO",
	"idDepartamento": 63,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edfa"
	},
	"id": 25758,
	"nombre": "SOPÓ",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edeb"
	},
	"id": 25878,
	"nombre": "VIOTÁ",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee18"
	},
	"id": 25823,
	"nombre": "TOPAIPÍ",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee13"
	},
	"id": 25862,
	"nombre": "VERGARA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee04"
	},
	"id": 25779,
	"nombre": "SUSA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee1d"
	},
	"id": 25815,
	"nombre": "TOCAIMA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eddc"
	},
	"id": 68001,
	"nombre": "BUCARAMANGA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee31"
	},
	"id": 5686,
	"nombre": "SANTA ROSA DE OSOS",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edd7"
	},
	"id": 63212,
	"nombre": "CÓRDOBA",
	"idDepartamento": 63,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edf5"
	},
	"id": 66594,
	"nombre": "QUINCHÍA",
	"idDepartamento": 66,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee0e"
	},
	"id": 5628,
	"nombre": "SABANALARGA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee27"
	},
	"id": 5667,
	"nombre": "SAN RAFAEL",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee4a"
	},
	"id": 5790,
	"nombre": "TARAZÁ",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee09"
	},
	"id": 25407,
	"nombre": "LENGUAZAQUE",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee40"
	},
	"id": 5837,
	"nombre": "TURBO",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee22"
	},
	"id": 5665,
	"nombre": "SAN PEDRO DE URABÁ",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee59"
	},
	"id": 5361,
	"nombre": "ITUANGO",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee63"
	},
	"id": 5854,
	"nombre": "VALDIVIA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee72"
	},
	"id": 15842,
	"nombre": "UMBITA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee54"
	},
	"id": 5490,
	"nombre": "NECOCLÍ",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee7c"
	},
	"id": 5411,
	"nombre": "LIBORINA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee6d"
	},
	"id": 5604,
	"nombre": "REMEDIOS",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee86"
	},
	"id": 15808,
	"nombre": "TINJACÁ",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee8b"
	},
	"id": 25148,
	"nombre": "CAPARRAPÍ",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee36"
	},
	"id": 5652,
	"nombre": "SAN FRANCISCO",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee95"
	},
	"id": 15837,
	"nombre": "TUTA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee4f"
	},
	"id": 5756,
	"nombre": "SONSÓN",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee68"
	},
	"id": 5250,
	"nombre": "EL BAGRE",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee81"
	},
	"id": 5425,
	"nombre": "MACEO",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeae"
	},
	"id": 25095,
	"nombre": "BITUIMA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee9a"
	},
	"id": 15764,
	"nombre": "SORACÁ",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee5e"
	},
	"id": 5847,
	"nombre": "URRAO",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeb3"
	},
	"id": 25299,
	"nombre": "GAMA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eec7"
	},
	"id": 5045,
	"nombre": "APARTADÓ",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeb8"
	},
	"id": 25297,
	"nombre": "GACHETÁ",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee90"
	},
	"id": 68092,
	"nombre": "BETULIA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eee0"
	},
	"id": 17495,
	"nombre": "NORCASIA",
	"idDepartamento": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eed1"
	},
	"id": 5038,
	"nombre": "ANGOSTURA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eea9"
	},
	"id": 25040,
	"nombre": "ANOLAIMA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeea"
	},
	"id": 17513,
	"nombre": "PÁCORA",
	"idDepartamento": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eec2"
	},
	"id": 25372,
	"nombre": "JUNÍN",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eef9"
	},
	"id": 5036,
	"nombre": "ANGELÓPOLIS",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eecc"
	},
	"id": 5042,
	"nombre": "SANTA FE DE ANTIOQUIA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eee5"
	},
	"id": 17867,
	"nombre": "VICTORIA",
	"idDepartamento": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eefe"
	},
	"id": 15455,
	"nombre": "MIRAFLORES",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef03"
	},
	"id": 15491,
	"nombre": "NOBSA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eef4"
	},
	"id": 17541,
	"nombre": "PENSILVANIA",
	"idDepartamento": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef12"
	},
	"id": 15090,
	"nombre": "BERBEO",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef17"
	},
	"id": 68689,
	"nombre": "SAN VICENTE DE CHUCURÍ",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef0d"
	},
	"id": 17653,
	"nombre": "SALAMINA",
	"idDepartamento": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef1c"
	},
	"id": 13654,
	"nombre": "SAN JACINTO",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef26"
	},
	"id": 15325,
	"nombre": "GUAYATÁ",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef2b"
	},
	"id": 17042,
	"nombre": "ANSERMA",
	"idDepartamento": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef30"
	},
	"id": 23682,
	"nombre": "SAN JOSE DE URE",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eebd"
	},
	"id": 25260,
	"nombre": "EL ROSAL",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef35"
	},
	"id": 23855,
	"nombre": "VALENCIA",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eed6"
	},
	"id": 5034,
	"nombre": "ANDES",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef44"
	},
	"id": 47053,
	"nombre": "ARACATACA",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef49"
	},
	"id": 13894,
	"nombre": "ZAMBRANO",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef4e"
	},
	"id": 13670,
	"nombre": "SAN PABLO",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef5d"
	},
	"id": 23580,
	"nombre": "PUERTO LIBERTADOR",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef62"
	},
	"id": 68673,
	"nombre": "SAN BENITO",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef67"
	},
	"id": 27615,
	"nombre": "RIOSUCIO",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef76"
	},
	"id": 27001,
	"nombre": "QUIBDO",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef7b"
	},
	"id": 68872,
	"nombre": "VILLANUEVA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef80"
	},
	"id": 13688,
	"nombre": "SANTA ROSA DEL SUR",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef53"
	},
	"id": 68895,
	"nombre": "ZAPATOCA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef8f"
	},
	"id": 13430,
	"nombre": "MAGANGUE",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef94"
	},
	"id": 27425,
	"nombre": "MEDIO ATRATO",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efa8"
	},
	"id": 66572,
	"nombre": "PUEBLO RICO",
	"idDepartamento": 66,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efad"
	},
	"id": 27077,
	"nombre": "BAJO BAUDO",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef9e"
	},
	"id": 17662,
	"nombre": "SAMANA",
	"idDepartamento": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efc1"
	},
	"id": 23417,
	"nombre": "LORICA",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efcb"
	},
	"id": 23815,
	"nombre": "TUCHÍN",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efd0"
	},
	"id": 70771,
	"nombre": "SUCRE",
	"idDepartamento": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efda"
	},
	"id": 20383,
	"nombre": "LA GLORIA",
	"idDepartamento": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efc6"
	},
	"id": 70678,
	"nombre": "SAN BENITO ABAD",
	"idDepartamento": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efe9"
	},
	"id": 85225,
	"nombre": "NUNCHÍA",
	"idDepartamento": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef3f"
	},
	"id": 23466,
	"nombre": "MONTELÍBANO",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efe4"
	},
	"id": 15299,
	"nombre": "GARAGOA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eff3"
	},
	"id": 85139,
	"nombre": "MANÍ",
	"idDepartamento": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eff8"
	},
	"id": 85010,
	"nombre": "AGUAZUL",
	"idDepartamento": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f002"
	},
	"id": 23555,
	"nombre": "PLANETA RICA",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef58"
	},
	"id": 47058,
	"nombre": "ARIGUANÍ",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f00c"
	},
	"id": 85162,
	"nombre": "MONTERREY",
	"idDepartamento": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f016"
	},
	"id": 85263,
	"nombre": "PORE",
	"idDepartamento": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f01b"
	},
	"id": 54680,
	"nombre": "SANTIAGO",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f011"
	},
	"id": 54743,
	"nombre": "SILOS",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef71"
	},
	"id": 13458,
	"nombre": "MONTECRISTO",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f025"
	},
	"id": 68468,
	"nombre": "MOLAGAVITA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f02a"
	},
	"id": 54820,
	"nombre": "TOLEDO",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f034"
	},
	"id": 81220,
	"nombre": "CRAVO NORTE",
	"idDepartamento": 81,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f02f"
	},
	"id": 54174,
	"nombre": "CHITAGÁ",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f03e"
	},
	"id": 81591,
	"nombre": "PUERTO RONDÓN",
	"idDepartamento": 81,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef8a"
	},
	"id": 13212,
	"nombre": "CORDOBA",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f048"
	},
	"id": 68160,
	"nombre": "CEPITÁ",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f04d"
	},
	"id": 11001,
	"nombre": "BOGOTÁ, D.C.",
	"idDepartamento": 11,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f043"
	},
	"id": 81065,
	"nombre": "ARAUQUITA",
	"idDepartamento": 81,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efa3"
	},
	"id": 76403,
	"nombre": "LA VICTORIA",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efbc"
	},
	"id": 27075,
	"nombre": "BAHIA SOLANO",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efd5"
	},
	"id": 13006,
	"nombre": "ACHI",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efee"
	},
	"id": 99624,
	"nombre": "SANTA ROSALÍA",
	"idDepartamento": 99,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f007"
	},
	"id": 54673,
	"nombre": "SAN CAYETANO",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f020"
	},
	"id": 15673,
	"nombre": "SAN MATEO",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb71"
	},
	"id": 76377,
	"nombre": "LA CUMBRE",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb7b"
	},
	"id": 76001,
	"nombre": "CALI",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb85"
	},
	"id": 73675,
	"nombre": "SAN ANTONIO",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb94"
	},
	"id": 73504,
	"nombre": "ORTEGA",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb9e"
	},
	"id": 73555,
	"nombre": "PLANADAS",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eba3"
	},
	"id": 52258,
	"nombre": "EL TABLÓN",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebbc"
	},
	"id": 52207,
	"nombre": "CONSACÁ",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebc6"
	},
	"id": 52354,
	"nombre": "IMUÉS",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb80"
	},
	"id": 19533,
	"nombre": "PIAMONTE",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebd0"
	},
	"id": 19050,
	"nombre": "ARGELIA",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb76"
	},
	"id": 76122,
	"nombre": "CAICEDONIA",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebdf"
	},
	"id": 25845,
	"nombre": "UNE",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb99"
	},
	"id": 73624,
	"nombre": "ROVIRA",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebe9"
	},
	"id": 50150,
	"nombre": "CASTILLA LA NUEVA",
	"idDepartamento": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebee"
	},
	"id": 19100,
	"nombre": "BOLÍVAR",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec07"
	},
	"id": 19698,
	"nombre": "SANTANDER DE QUILICHAO",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec11"
	},
	"id": 19693,
	"nombre": "SAN SEBASTIÁN",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eba8"
	},
	"id": 52694,
	"nombre": "SAN PEDRO DE CARTAGO (CARTAGO)",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec20"
	},
	"id": 19364,
	"nombre": "JAMBALÓ",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec2a"
	},
	"id": 41524,
	"nombre": "PALERMO",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec39"
	},
	"id": 41378,
	"nombre": "LA ARGENTINA",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebe4"
	},
	"id": 50270,
	"nombre": "EL DORADO",
	"idDepartamento": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec5c"
	},
	"id": 41020,
	"nombre": "ALGECIRAS",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec16"
	},
	"id": 19517,
	"nombre": "PÁEZ (BELALCÁZAR)",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec0c"
	},
	"id": 19821,
	"nombre": "TORIBÍO",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec52"
	},
	"id": 41770,
	"nombre": "SUAZA",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec2f"
	},
	"id": 41078,
	"nombre": "BARAYA",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec6b"
	},
	"id": 25335,
	"nombre": "GUAYABETAL",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec48"
	},
	"id": 41503,
	"nombre": "OPORAPA",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec84"
	},
	"id": 86571,
	"nombre": "PUERTO GUZMAN",
	"idDepartamento": 86,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec3e"
	},
	"id": 41660,
	"nombre": "SALADOBLANCO",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec57"
	},
	"id": 41306,
	"nombre": "GIGANTE",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec7a"
	},
	"id": 18410,
	"nombre": "LA MONTAÑITA",
	"idDepartamento": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec93"
	},
	"id": 86001,
	"nombre": "MOCOA",
	"idDepartamento": 86,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eca7"
	},
	"id": 86760,
	"nombre": "SANTIAGO",
	"idDepartamento": 86,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecc0"
	},
	"id": 18460,
	"nombre": "MILAN",
	"idDepartamento": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecc5"
	},
	"id": 18479,
	"nombre": "ALBANIA",
	"idDepartamento": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec1b"
	},
	"id": 19397,
	"nombre": "LA VEGA",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecbb"
	},
	"id": 52685,
	"nombre": "SAN BERNARDO",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eccf"
	},
	"id": 50325,
	"nombre": "MAPIRIPÁN",
	"idDepartamento": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eced"
	},
	"id": 85300,
	"nombre": "SABANALARGA",
	"idDepartamento": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ece8"
	},
	"id": 52390,
	"nombre": "LA TOLA",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed01"
	},
	"id": 68655,
	"nombre": "SABANA DE TORRES",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed06"
	},
	"id": 19318,
	"nombre": "GUAPI",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed1a"
	},
	"id": 54398,
	"nombre": "LA PLAYA",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed1f"
	},
	"id": 68780,
	"nombre": "SURATÁ",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecd9"
	},
	"id": 41791,
	"nombre": "TARQUI",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed33"
	},
	"id": 70233,
	"nombre": "EL ROBLE",
	"idDepartamento": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecb1"
	},
	"id": 86755,
	"nombre": "SAN FRANCISCO",
	"idDepartamento": 86,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed0b"
	},
	"id": 70204,
	"nombre": "COLOSÓ",
	"idDepartamento": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed4c"
	},
	"id": 13873,
	"nombre": "VILLANUEVA",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ece3"
	},
	"id": 73585,
	"nombre": "PURIFICACIÓN",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecfc"
	},
	"id": 94001,
	"nombre": "INIRIDA",
	"idDepartamento": 94,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed65"
	},
	"id": 73043,
	"nombre": "ANZOÁTEGUI",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed15"
	},
	"id": 68572,
	"nombre": "PUENTE NACIONAL",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed56"
	},
	"id": 13042,
	"nombre": "ARENAL",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed51"
	},
	"id": 73124,
	"nombre": "CAJAMARCA",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed2e"
	},
	"id": 70717,
	"nombre": "SAN PEDRO",
	"idDepartamento": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed6f"
	},
	"id": 13780,
	"nombre": "TALAIGUA NUEVO",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed47"
	},
	"id": 73443,
	"nombre": "MARIQUITA",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed7e"
	},
	"id": 13468,
	"nombre": "MOMPÓS",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed88"
	},
	"id": 23068,
	"nombre": "AYAPEL",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecde"
	},
	"id": 76736,
	"nombre": "SEVILLA",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed97"
	},
	"id": 20710,
	"nombre": "SAN ALBERTO",
	"idDepartamento": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed79"
	},
	"id": 13744,
	"nombre": "SIMITÍ",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eda1"
	},
	"id": 54344,
	"nombre": "HACARÍ",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed83"
	},
	"id": 20011,
	"nombre": "AGUACHICA",
	"idDepartamento": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edba"
	},
	"id": 68235,
	"nombre": "EL CARMEN DE CHUCURÍ",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edc9"
	},
	"id": 66170,
	"nombre": "DOSQUEBRADAS",
	"idDepartamento": 66,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edd3"
	},
	"id": 63272,
	"nombre": "FILANDIA",
	"idDepartamento": 63,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eddd"
	},
	"id": 63594,
	"nombre": "QUIMBAYA",
	"idDepartamento": 63,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ede2"
	},
	"id": 25486,
	"nombre": "NEMOCÓN",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed29"
	},
	"id": 76246,
	"nombre": "EL CAIRO",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edb5"
	},
	"id": 68121,
	"nombre": "CABRERA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed42"
	},
	"id": 5209,
	"nombre": "CONCORDIA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edfb"
	},
	"id": 25489,
	"nombre": "NIMAIMA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edf6"
	},
	"id": 25885,
	"nombre": "YACOPÍ",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edce"
	},
	"id": 66075,
	"nombre": "BALBOA",
	"idDepartamento": 66,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee05"
	},
	"id": 25596,
	"nombre": "QUIPILE",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed74"
	},
	"id": 20770,
	"nombre": "SAN MARTÍN",
	"idDepartamento": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee0f"
	},
	"id": 25851,
	"nombre": "ÚTICA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee1e"
	},
	"id": 25839,
	"nombre": "UBALÁ",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ede7"
	},
	"id": 25426,
	"nombre": "MACHETA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eda6"
	},
	"id": 54385,
	"nombre": "LA ESPERANZA",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee2d"
	},
	"id": 5679,
	"nombre": "SANTA BÁRBARA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee37"
	},
	"id": 5483,
	"nombre": "NARIÑO",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee00"
	},
	"id": 25736,
	"nombre": "SESQUILÉ",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee46"
	},
	"id": 5789,
	"nombre": "TÁMESIS",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee50"
	},
	"id": 5792,
	"nombre": "TARSO",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edd8"
	},
	"id": 63130,
	"nombre": "CALARCÁ",
	"idDepartamento": 63,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee41"
	},
	"id": 5873,
	"nombre": "VIGIA DEL FUERTE",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edf1"
	},
	"id": 25394,
	"nombre": "LA PALMA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee5f"
	},
	"id": 5360,
	"nombre": "ITAGÜÍ",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee0a"
	},
	"id": 25599,
	"nombre": "APULO",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee69"
	},
	"id": 5579,
	"nombre": "PUERTO BERRÍO",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee23"
	},
	"id": 5761,
	"nombre": "SOPETRÁN",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee5a"
	},
	"id": 5313,
	"nombre": "GRANADA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee78"
	},
	"id": 5440,
	"nombre": "MARINILLA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee32"
	},
	"id": 5660,
	"nombre": "SAN LUIS",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee82"
	},
	"id": 15667,
	"nombre": "SAN LUIS DE GACENO",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee73"
	},
	"id": 68020,
	"nombre": "ALBANIA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee91"
	},
	"id": 15835,
	"nombre": "TURMEQUÉ",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee55"
	},
	"id": 5376,
	"nombre": "LA CEJA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee4b"
	},
	"id": 5690,
	"nombre": "SANTO DOMINGO",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee9b"
	},
	"id": 15681,
	"nombre": "SAN PABLO DE BORBUR",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeaa"
	},
	"id": 25123,
	"nombre": "CACHIPAY",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee87"
	},
	"id": 15621,
	"nombre": "RONDÓN",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeb4"
	},
	"id": 25317,
	"nombre": "GUACHETÁ",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eea0"
	},
	"id": 25154,
	"nombre": "CARMEN DE CARUPA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eec3"
	},
	"id": 25286,
	"nombre": "FUNZA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eea5"
	},
	"id": 25178,
	"nombre": "CHIPAQUE",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeb9"
	},
	"id": 25322,
	"nombre": "GUASCA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eecd"
	},
	"id": 5093,
	"nombre": "BETULIA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee7d"
	},
	"id": 5467,
	"nombre": "MONTEBELLO",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eed2"
	},
	"id": 5040,
	"nombre": "ANORÍ",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eedc"
	},
	"id": 5031,
	"nombre": "AMALFI",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eebe"
	},
	"id": 15572,
	"nombre": "PUERTO BOYACÁ",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eee6"
	},
	"id": 17873,
	"nombre": "VILLAMARIA",
	"idDepartamento": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eed7"
	},
	"id": 5001,
	"nombre": "MEDELLÍN",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee96"
	},
	"id": 5240,
	"nombre": "EBÉJICO",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeeb"
	},
	"id": 17272,
	"nombre": "FILADELFIA",
	"idDepartamento": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeff"
	},
	"id": 15204,
	"nombre": "CÓMBITA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef09"
	},
	"id": 15511,
	"nombre": "PACHAVITA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef0e"
	},
	"id": 15293,
	"nombre": "GACHANTIVÁ",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeaf"
	},
	"id": 25175,
	"nombre": "CHÍA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef27"
	},
	"id": 15322,
	"nombre": "GUATEQUE",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef31"
	},
	"id": 23807,
	"nombre": "TIERRALTA",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef40"
	},
	"id": 15542,
	"nombre": "PESCA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef4f"
	},
	"id": 47745,
	"nombre": "SITIONUEVO",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef59"
	},
	"id": 47555,
	"nombre": "PLATO",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eee1"
	},
	"id": 17001,
	"nombre": "MANIZALES",
	"idDepartamento": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef63"
	},
	"id": 27600,
	"nombre": "RIO QUITO",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef72"
	},
	"id": 27430,
	"nombre": "MEDIO BAUDO",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef7c"
	},
	"id": 13490,
	"nombre": "RIO VIEJO",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eefa"
	},
	"id": 5086,
	"nombre": "BELMIRA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef95"
	},
	"id": 13268,
	"nombre": "EL PENON",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef86"
	},
	"id": 27099,
	"nombre": "BOJAYA",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef13"
	},
	"id": 17088,
	"nombre": "BELALCAZAR",
	"idDepartamento": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef81"
	},
	"id": 13549,
	"nombre": "PINILLOS",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efa4"
	},
	"id": 27025,
	"nombre": "ALTO BAUDO",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efbd"
	},
	"id": 13160,
	"nombre": "CANTAGALLO",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef2c"
	},
	"id": 13244,
	"nombre": "EL CARMEN DE BOLÍVAR",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef45"
	},
	"id": 8372,
	"nombre": "JUAN DE ACOSTA",
	"idDepartamento": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efd6"
	},
	"id": 76400,
	"nombre": "LA UNIÓN",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef5e"
	},
	"id": 68324,
	"nombre": "GUAVATÁ",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efcc"
	},
	"id": 66400,
	"nombre": "LA VIRGINIA",
	"idDepartamento": 66,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efe5"
	},
	"id": 15632,
	"nombre": "SABOYÁ",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef90"
	},
	"id": 27372,
	"nombre": "JURADO",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8effe"
	},
	"id": 23570,
	"nombre": "PUEBLO NUEVO",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f008"
	},
	"id": 54874,
	"nombre": "VILLA DEL ROSARIO",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efa9"
	},
	"id": 73870,
	"nombre": "VILLAHERMOSA",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f017"
	},
	"id": 54660,
	"nombre": "SALAZAR",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f021"
	},
	"id": 15755,
	"nombre": "SOCOTÁ",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efdb"
	},
	"id": 20250,
	"nombre": "EL PASO",
	"idDepartamento": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f030"
	},
	"id": 54223,
	"nombre": "CUCUTILLA",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f03a"
	},
	"id": 54480,
	"nombre": "MUTISCUA",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eff4"
	},
	"id": 85230,
	"nombre": "OROCUÉ",
	"idDepartamento": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f049"
	},
	"id": 15047,
	"nombre": "AQUITANIA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f057"
	},
	"id": 25594,
	"nombre": "QUETAME",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f026"
	},
	"id": 15774,
	"nombre": "SUSACÓN",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f066"
	},
	"id": 85400,
	"nombre": "TAMARA",
	"idDepartamento": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f070"
	},
	"id": 19760,
	"nombre": "SOTARA",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f05c"
	},
	"id": 44847,
	"nombre": "URIBIA",
	"idDepartamento": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f075"
	},
	"id": 44855,
	"nombre": "URUMITA",
	"idDepartamento": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efc7"
	},
	"id": 68406,
	"nombre": "LEBRIJA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efea"
	},
	"id": 85250,
	"nombre": "PAZ DE ARIPORO",
	"idDepartamento": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f003"
	},
	"id": 68669,
	"nombre": "SAN ANDRÉS",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f01c"
	},
	"id": 68147,
	"nombre": "CAPITANEJO",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f035"
	},
	"id": 54313,
	"nombre": "GRAMALOTE",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f052"
	},
	"id": 85325,
	"nombre": "SAN LUIS DE PALENQUE",
	"idDepartamento": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f06b"
	},
	"id": 5380,
	"nombre": "LA ESTRELLA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f02b"
	},
	"id": 54261,
	"nombre": "EL ZULIA",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f061"
	},
	"id": 68152,
	"nombre": "CARCASÍ",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f07a"
	},
	"id": 50110,
	"nombre": "BARRANCA DE UPÍA",
	"idDepartamento": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f089"
	},
	"id": 19137,
	"nombre": "CALDONO",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f08e"
	},
	"id": 19450,
	"nombre": "MERCADERES",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f0a7"
	},
	"id": 73770,
	"nombre": "SUÁREZ",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f0bb"
	},
	"id": 73236,
	"nombre": "DOLORES",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f0cf"
	},
	"id": 52051,
	"nombre": "ARBOLEDA (BERRUECOS)",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f0d4"
	},
	"id": 52260,
	"nombre": "EL TAMBO",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f0ca"
	},
	"id": 52203,
	"nombre": "COLÓN (GÉNOVA)",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f11f"
	},
	"id": 19809,
	"nombre": "TIMBIQUÍ",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f133"
	},
	"id": 41298,
	"nombre": "GARZÓN",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f12e"
	},
	"id": 19290,
	"nombre": "FLORENCIA",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f138"
	},
	"id": 41349,
	"nombre": "HOBO",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f147"
	},
	"id": 41132,
	"nombre": "CAMPOALEGRE",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f0c5"
	},
	"id": 73275,
	"nombre": "FLANDES",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f0f2"
	},
	"id": 25649,
	"nombre": "SAN BERNARDO",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f10b"
	},
	"id": 41359,
	"nombre": "ISNOS",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f197"
	},
	"id": 86320,
	"nombre": "ORITO",
	"idDepartamento": 86,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f110"
	},
	"id": 50606,
	"nombre": "RESTREPO",
	"idDepartamento": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f13d"
	},
	"id": 41319,
	"nombre": "GUADALUPE",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f129"
	},
	"id": 19256,
	"nombre": "EL TAMBO",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f183"
	},
	"id": 18247,
	"nombre": "EL DONCELLO",
	"idDepartamento": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f174"
	},
	"id": 18205,
	"nombre": "CURILLO",
	"idDepartamento": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f1a1"
	},
	"id": 19622,
	"nombre": "ROSAS",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f1ce"
	},
	"id": 52399,
	"nombre": "LA UNION",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f200"
	},
	"id": 68547,
	"nombre": "PIEDECUESTA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f1c9"
	},
	"id": 52356,
	"nombre": "IPIALES",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f1d8"
	},
	"id": 18256,
	"nombre": "EL PAUJIL",
	"idDepartamento": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f1f1"
	},
	"id": 52696,
	"nombre": "NARIÑO",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f232"
	},
	"id": 5154,
	"nombre": "CAUCASIA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f1fb"
	},
	"id": 95025,
	"nombre": "EL RETORNO",
	"idDepartamento": 95,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f20a"
	},
	"id": 94343,
	"nombre": "BARRANCO MINA",
	"idDepartamento": 94,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f223"
	},
	"id": 70670,
	"nombre": "SAMPUÉS",
	"idDepartamento": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f24b"
	},
	"id": 73861,
	"nombre": "VENADILLO",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f1d3"
	},
	"id": 52022,
	"nombre": "ALDANA",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f264"
	},
	"id": 5172,
	"nombre": "CHIGORODÓ",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f26e"
	},
	"id": 13433,
	"nombre": "MAHATES",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f246"
	},
	"id": 70230,
	"nombre": "CHALÁN",
	"idDepartamento": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f287"
	},
	"id": 20228,
	"nombre": "CURUMANÍ",
	"idDepartamento": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2af"
	},
	"id": 68500,
	"nombre": "OIBA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f291"
	},
	"id": 20310,
	"nombre": "GONZÁLEZ",
	"idDepartamento": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2aa"
	},
	"id": 68211,
	"nombre": "CONTRATACIÓN",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f205"
	},
	"id": 68533,
	"nombre": "PÁRAMO",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2c3"
	},
	"id": 68498,
	"nombre": "OCAMONTE",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f20f"
	},
	"id": 95001,
	"nombre": "SAN JOSÉ DEL GUAVIARE",
	"idDepartamento": 95,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f282"
	},
	"id": 47960,
	"nombre": "ZAPAYÁN",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f29b"
	},
	"id": 54003,
	"nombre": "ÁBREGO",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f228"
	},
	"id": 68573,
	"nombre": "PUERTO PARRA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f241"
	},
	"id": 5125,
	"nombre": "CAICEDO",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2a5"
	},
	"id": 20443,
	"nombre": "MANAURE BALCÓN DEL CESAR",
	"idDepartamento": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2cd"
	},
	"id": 68245,
	"nombre": "EL GUACAMAYO",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f318"
	},
	"id": 25398,
	"nombre": "LA PEÑA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f331"
	},
	"id": 25645,
	"nombre": "SAN ANTONIO DEL TEQUENDAMA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f34a"
	},
	"id": 5858,
	"nombre": "VEGACHÍ",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2eb"
	},
	"id": 68418,
	"nombre": "LOS SANTOS",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f37c"
	},
	"id": 5543,
	"nombre": "PEQUE",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f304"
	},
	"id": 25873,
	"nombre": "VILLAPINZÓN",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f395"
	},
	"id": 15686,
	"nombre": "SANTANA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3ae"
	},
	"id": 15693,
	"nombre": "SANTA ROSA DE VITERBO",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3f9"
	},
	"id": 17444,
	"nombre": "MARQUETALIA",
	"idDepartamento": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f412"
	},
	"id": 15135,
	"nombre": "CAMPOHERMOSO",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f368"
	},
	"id": 5318,
	"nombre": "GUARNE",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f444"
	},
	"id": 68385,
	"nombre": "LANDÁZURI",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f45d"
	},
	"id": 47675,
	"nombre": "SALAMINA",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f39a"
	},
	"id": 15806,
	"nombre": "TIBASOSA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3b3"
	},
	"id": 25258,
	"nombre": "EL PEÑÓN",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3cc"
	},
	"id": 25326,
	"nombre": "GUATAVITA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4da"
	},
	"id": 70708,
	"nombre": "SAN MARCOS",
	"idDepartamento": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3e5"
	},
	"id": 5051,
	"nombre": "ARBOLETES",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4f3"
	},
	"id": 20032,
	"nombre": "ASTREA",
	"idDepartamento": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3fe"
	},
	"id": 5079,
	"nombre": "BARBOSA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f50c"
	},
	"id": 23419,
	"nombre": "LOS CORDOBAS",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f417"
	},
	"id": 15172,
	"nombre": "CHINAVITA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f53e"
	},
	"id": 54377,
	"nombre": "LABATECA",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f313"
	},
	"id": 25518,
	"nombre": "PAIME",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f32c"
	},
	"id": 25841,
	"nombre": "UBAQUE",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f430"
	},
	"id": 13001,
	"nombre": "CARTAGENA DE INDIAS",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f557"
	},
	"id": 54099,
	"nombre": "BOCHALEMA",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f35e"
	},
	"id": 5697,
	"nombre": "SANTUARIO",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f377"
	},
	"id": 5541,
	"nombre": "PEÑOL",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f58a"
	},
	"id": 19418,
	"nombre": "LÓPEZ DE MICAY",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5bc"
	},
	"id": 76318,
	"nombre": "GUACARÍ",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f462"
	},
	"id": 8549,
	"nombre": "PIOJO",
	"idDepartamento": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4ad"
	},
	"id": 17442,
	"nombre": "MARMATO",
	"idDepartamento": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4f8"
	},
	"id": 85001,
	"nombre": "YOPAL",
	"idDepartamento": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f511"
	},
	"id": 23090,
	"nombre": "CANALETE",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f52a"
	},
	"id": 54599,
	"nombre": "RAGONVALIA",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f543"
	},
	"id": 81794,
	"nombre": "TAME",
	"idDepartamento": 81,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f576"
	},
	"id": 15790,
	"nombre": "TASCO",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5c1"
	},
	"id": 76563,
	"nombre": "PRADERA",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5da"
	},
	"id": 73622,
	"nombre": "RONCESVALLES",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5f3"
	},
	"id": 52405,
	"nombre": "LEIVA",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f60c"
	},
	"id": 52215,
	"nombre": "CÓRDOBA",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f6a2"
	},
	"id": 41872,
	"nombre": "VILLAVIEJA",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f706"
	},
	"id": 52378,
	"nombre": "LA CRUZ",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f751"
	},
	"id": 94887,
	"nombre": "PANÁ PANÁ",
	"idDepartamento": 94,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f620"
	},
	"id": 50400,
	"nombre": "LEJANÍAS",
	"idDepartamento": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f639"
	},
	"id": 19110,
	"nombre": "BUENOS AIRES",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f652"
	},
	"id": 19701,
	"nombre": "SANTA ROSA",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f6e8"
	},
	"id": 41483,
	"nombre": "NÁTAGA",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f765"
	},
	"id": 54720,
	"nombre": "SARDINATA",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3c2"
	},
	"id": 25368,
	"nombre": "JERUSALÉN",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3f4"
	},
	"id": 17433,
	"nombre": "MANZANARES",
	"idDepartamento": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f48a"
	},
	"id": 13657,
	"nombre": "SAN JUAN NEPOMUCENO",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4a3"
	},
	"id": 27450,
	"nombre": "MEDIO SAN JUAN",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4bc"
	},
	"id": 66687,
	"nombre": "SANTUARIO",
	"idDepartamento": 66,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4d5"
	},
	"id": 70473,
	"nombre": "MORROA",
	"idDepartamento": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4ee"
	},
	"id": 17614,
	"nombre": "RIOSUCIO",
	"idDepartamento": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f552"
	},
	"id": 81001,
	"nombre": "ARAUCA",
	"idDepartamento": 81,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5e9"
	},
	"id": 52788,
	"nombre": "TANGUA",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f602"
	},
	"id": 50223,
	"nombre": "SAN LUIS DE CUBARRAL",
	"idDepartamento": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f67f"
	},
	"id": 41016,
	"nombre": "AIPE",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f6fc"
	},
	"id": 52036,
	"nombre": "ANCUYA",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f747"
	},
	"id": 76520,
	"nombre": "PALMIRA",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f309"
	},
	"id": 25740,
	"nombre": "SIBATÉ",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f322"
	},
	"id": 5649,
	"nombre": "SAN CARLOS",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f33b"
	},
	"id": 5674,
	"nombre": "SAN VICENTE",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f354"
	},
	"id": 5736,
	"nombre": "SEGOVIA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3b8"
	},
	"id": 25035,
	"nombre": "ANAPOIMA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3d1"
	},
	"id": 25245,
	"nombre": "EL COLEGIO",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f403"
	},
	"id": 5055,
	"nombre": "ARGELIA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f41c"
	},
	"id": 15480,
	"nombre": "MUZO",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f44e"
	},
	"id": 54250,
	"nombre": "EL TARRA",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f480"
	},
	"id": 13650,
	"nombre": "SAN FERNANDO",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f499"
	},
	"id": 13300,
	"nombre": "HATILLO DE LOBA",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f516"
	},
	"id": 54518,
	"nombre": "PAMPLONA",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f52f"
	},
	"id": 15550,
	"nombre": "PISBA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5c6"
	},
	"id": 76834,
	"nombre": "TULUÁ",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2dc"
	},
	"id": 66088,
	"nombre": "BELÉN DE UMBRÍA",
	"idDepartamento": 66,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2f5"
	},
	"id": 25438,
	"nombre": "MEDINA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f30e"
	},
	"id": 25718,
	"nombre": "SASAIMA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f643"
	},
	"id": 41548,
	"nombre": "PITAL",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f68e"
	},
	"id": 41530,
	"nombre": "PALESTINA",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f372"
	},
	"id": 5842,
	"nombre": "URAMITA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f6a7"
	},
	"id": 41807,
	"nombre": "TIMANÁ",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3bd"
	},
	"id": 25181,
	"nombre": "CHOACHÍ",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3d6"
	},
	"id": 25214,
	"nombre": "COTA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f421"
	},
	"id": 17777,
	"nombre": "SUPIA",
	"idDepartamento": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f43a"
	},
	"id": 15109,
	"nombre": "BUENAVISTA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f46c"
	},
	"id": 68745,
	"nombre": "SIMACOTA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f485"
	},
	"id": 27495,
	"nombre": "NUQUI",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4b7"
	},
	"id": 76895,
	"nombre": "ZARZAL",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4d0"
	},
	"id": 15367,
	"nombre": "JENESANO",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4e9"
	},
	"id": 20178,
	"nombre": "CHIRIGUANA",
	"idDepartamento": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f502"
	},
	"id": 54405,
	"nombre": "LOS PATIOS",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f51b"
	},
	"id": 85125,
	"nombre": "HATO COROZAL",
	"idDepartamento": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f534"
	},
	"id": 54051,
	"nombre": "ARBOLEDAS",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f54d"
	},
	"id": 15218,
	"nombre": "COVARACHÍA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f567"
	},
	"id": 50124,
	"nombre": "CABUYARO",
	"idDepartamento": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5fd"
	},
	"id": 73483,
	"nombre": "NATAGAIMA",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f729"
	},
	"id": 52490,
	"nombre": "OLAYA HERRERA",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f742"
	},
	"id": 68773,
	"nombre": "SUCRE",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f75b"
	},
	"id": 70001,
	"nombre": "SINCELEJO",
	"idDepartamento": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb6d"
	},
	"id": 76890,
	"nombre": "YOTOCO",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb7c"
	},
	"id": 19455,
	"nombre": "MIRANDA",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eb90"
	},
	"id": 73319,
	"nombre": "GUAMO",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eba9"
	},
	"id": 52835,
	"nombre": "TUMACO",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebcc"
	},
	"id": 52317,
	"nombre": "GUACHUCAL",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebae"
	},
	"id": 52678,
	"nombre": "SAMANIEGO",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebe5"
	},
	"id": 50251,
	"nombre": "EL CASTILLO",
	"idDepartamento": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec7b"
	},
	"id": 18610,
	"nombre": "SAN JOSÉ DEL FRAGUA",
	"idDepartamento": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec08"
	},
	"id": 19585,
	"nombre": "AREA EN LITIGIO",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecc6"
	},
	"id": 52110,
	"nombre": "BUESACO",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec5d"
	},
	"id": 41885,
	"nombre": "YAGUARÁ",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed11"
	},
	"id": 73408,
	"nombre": "LÉRIDA",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eca8"
	},
	"id": 86865,
	"nombre": "VALLE DEL GUAMUEZ",
	"idDepartamento": 86,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecc1"
	},
	"id": 52435,
	"nombre": "MALLAMA",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed43"
	},
	"id": 5480,
	"nombre": "MUTATÁ",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed5c"
	},
	"id": 5885,
	"nombre": "YALÍ",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed75"
	},
	"id": 23574,
	"nombre": "PUERTO ESCONDIDO",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed8e"
	},
	"id": 47318,
	"nombre": "GUAMAL",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eda7"
	},
	"id": 54245,
	"nombre": "EL CARMEN",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed34"
	},
	"id": 76622,
	"nombre": "ROLDANILLO",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed4d"
	},
	"id": 5206,
	"nombre": "CONCEPCIÓN",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edc0"
	},
	"id": 68344,
	"nombre": "HATO",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eda2"
	},
	"id": 68229,
	"nombre": "CURITÍ",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edd4"
	},
	"id": 66682,
	"nombre": "SANTA ROSA DE CABAL",
	"idDepartamento": 66,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee0b"
	},
	"id": 25535,
	"nombre": "PASCA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee06"
	},
	"id": 25530,
	"nombre": "PARATEBUENO",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee24"
	},
	"id": 5670,
	"nombre": "SAN ROQUE",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee1f"
	},
	"id": 25793,
	"nombre": "TAUSA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edfc"
	},
	"id": 25488,
	"nombre": "NILO",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee15"
	},
	"id": 25473,
	"nombre": "MOSQUERA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecd5"
	},
	"id": 50683,
	"nombre": "SAN JUAN DE ARAMA",
	"idDepartamento": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee51"
	},
	"id": 5308,
	"nombre": "GIRARDOTA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee6a"
	},
	"id": 27787,
	"nombre": "TADÓ",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee83"
	},
	"id": 15763,
	"nombre": "SOTAQUIRÁ",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee9c"
	},
	"id": 15832,
	"nombre": "TUNUNGUÁ",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed52"
	},
	"id": 5145,
	"nombre": "CARAMANTA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeb5"
	},
	"id": 5030,
	"nombre": "AMAGÁ",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeba"
	},
	"id": 5021,
	"nombre": "ALEJANDRÍA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed9d"
	},
	"id": 68298,
	"nombre": "GAMBITA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edb6"
	},
	"id": 66440,
	"nombre": "MARSELLA",
	"idDepartamento": 66,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eee7"
	},
	"id": 17174,
	"nombre": "CHINCHINÁ",
	"idDepartamento": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeec"
	},
	"id": 17013,
	"nombre": "AGUADAS",
	"idDepartamento": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edcf"
	},
	"id": 66383,
	"nombre": "LA CELIA",
	"idDepartamento": 66,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ede8"
	},
	"id": 25436,
	"nombre": "MANTA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef00"
	},
	"id": 15514,
	"nombre": "PÁEZ",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef05"
	},
	"id": 15469,
	"nombre": "MONIQUIRÁ",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef19"
	},
	"id": 5120,
	"nombre": "CÁCERES",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee33"
	},
	"id": 5664,
	"nombre": "SAN PEDRO",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee92"
	},
	"id": 15664,
	"nombre": "SAN JOSÉ DE PARE",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeab"
	},
	"id": 25183,
	"nombre": "CHOCONTÁ",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee7e"
	},
	"id": 5368,
	"nombre": "JERICÓ",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef0f"
	},
	"id": 15442,
	"nombre": "MARIPÍ",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efb4"
	},
	"id": 13052,
	"nombre": "ARJONA",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef4b"
	},
	"id": 13655,
	"nombre": "SAN JACINTO DEL CAUCA",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef14"
	},
	"id": 15272,
	"nombre": "FIRAVITOBA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef41"
	},
	"id": 27160,
	"nombre": "CÉRTEGUI",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eccb"
	},
	"id": 18785,
	"nombre": "SOLITA",
	"idDepartamento": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efff"
	},
	"id": 70429,
	"nombre": "MAJAGUAL",
	"idDepartamento": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecfd"
	},
	"id": 50450,
	"nombre": "PUERTO CONCORDIA",
	"idDepartamento": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef5f"
	},
	"id": 47692,
	"nombre": "SAN SEBASTIÁN DE BUENAVISTA",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed48"
	},
	"id": 13836,
	"nombre": "TURBACO",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f04a"
	},
	"id": 15097,
	"nombre": "BOAVITA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efdc"
	},
	"id": 15531,
	"nombre": "PAUNA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edac"
	},
	"id": 47268,
	"nombre": "EL RETÉN",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eff5"
	},
	"id": 68820,
	"nombre": "TONA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef96"
	},
	"id": 13222,
	"nombre": "CLEMENCIA",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f027"
	},
	"id": 15753,
	"nombre": "SOATÁ",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efc8"
	},
	"id": 15212,
	"nombre": "COPER",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f040"
	},
	"id": 81300,
	"nombre": "FORTUL",
	"idDepartamento": 81,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efe1"
	},
	"id": 68307,
	"nombre": "GIRON",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee74"
	},
	"id": 68077,
	"nombre": "BARBOSA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee8d"
	},
	"id": 15814,
	"nombre": "TOCA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f05d"
	},
	"id": 15368,
	"nombre": "JERICÓ",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eea6"
	},
	"id": 25279,
	"nombre": "FOMEQUE",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef5a"
	},
	"id": 8421,
	"nombre": "LURUACO",
	"idDepartamento": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eed8"
	},
	"id": 5004,
	"nombre": "ABRIAQUÍ",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eef1"
	},
	"id": 17380,
	"nombre": "LA DORADA",
	"idDepartamento": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef23"
	},
	"id": 70713,
	"nombre": "SAN ONOFRE",
	"idDepartamento": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eff0"
	},
	"id": 85410,
	"nombre": "TAURAMENA",
	"idDepartamento": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f03b"
	},
	"id": 81736,
	"nombre": "SARAVENA",
	"idDepartamento": 81,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f053"
	},
	"id": 50590,
	"nombre": "PUERTO RICO",
	"idDepartamento": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efd2"
	},
	"id": 76250,
	"nombre": "EL DOVIO",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f06c"
	},
	"id": 68867,
	"nombre": "VETAS",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efeb"
	},
	"id": 85430,
	"nombre": "TRINIDAD",
	"idDepartamento": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f036"
	},
	"id": 68684,
	"nombre": "SAN JOSÉ DE MIRANDA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f094"
	},
	"id": 73873,
	"nombre": "VILLARRICA",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f0d5"
	},
	"id": 52352,
	"nombre": "ILES",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f080"
	},
	"id": 76606,
	"nombre": "RESTREPO",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f09e"
	},
	"id": 73352,
	"nombre": "ICONONZO",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f0d0"
	},
	"id": 52224,
	"nombre": "CUASPUD (CARLOSAMA)",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f102"
	},
	"id": 19130,
	"nombre": "CAJIBÍO",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f14d"
	},
	"id": 41615,
	"nombre": "RIVERA",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f0cb"
	},
	"id": 52019,
	"nombre": "ALBÁN (SAN JOSÉ)",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f201"
	},
	"id": 68524,
	"nombre": "PALMAS DEL SOCORRO",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f1ca"
	},
	"id": 52573,
	"nombre": "PUERRES",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f21a"
	},
	"id": 76113,
	"nombre": "BUGA LA GRANDE",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f12f"
	},
	"id": 19573,
	"nombre": "PUERTO TEJADA",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f24c"
	},
	"id": 20570,
	"nombre": "PUEBLO BELLO",
	"idDepartamento": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2a1"
	},
	"id": 20517,
	"nombre": "PAILITAS",
	"idDepartamento": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f297"
	},
	"id": 47551,
	"nombre": "PIVIJAY",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f242"
	},
	"id": 54871,
	"nombre": "VILLA CARO",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f189"
	},
	"id": 25120,
	"nombre": "CABRERA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2c9"
	},
	"id": 68250,
	"nombre": "EL PEÑÓN",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f274"
	},
	"id": 73520,
	"nombre": "PALOCABILDO",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2c4"
	},
	"id": 68276,
	"nombre": "FLORIDABLANCA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f238"
	},
	"id": 5190,
	"nombre": "CISNEROS",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f26a"
	},
	"id": 73026,
	"nombre": "ALVARADO",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f283"
	},
	"id": 20750,
	"nombre": "SAN DIEGO",
	"idDepartamento": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2dd"
	},
	"id": 66045,
	"nombre": "APÍA",
	"idDepartamento": 66,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f30a"
	},
	"id": 25769,
	"nombre": "SUBACHOQUE",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f30f"
	},
	"id": 25777,
	"nombre": "SUPATÁ",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f341"
	},
	"id": 5656,
	"nombre": "SAN JERÓNIMO",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f373"
	},
	"id": 5856,
	"nombre": "VALPARAISO",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3a5"
	},
	"id": 27580,
	"nombre": "RIO IRÓ (SANTA RITA)",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3b9"
	},
	"id": 15861,
	"nombre": "VENTAQUEMADA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f436"
	},
	"id": 15516,
	"nombre": "PAIPA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f454"
	},
	"id": 47189,
	"nombre": "CIÉNAGA",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f468"
	},
	"id": 47460,
	"nombre": "NUEVA GRANADA",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4e5"
	},
	"id": 27006,
	"nombre": "ACANDI",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f517"
	},
	"id": 54810,
	"nombre": "TIBÚ",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f51c"
	},
	"id": 54001,
	"nombre": "CÚCUTA",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f563"
	},
	"id": 15317,
	"nombre": "GUACAMAYAS",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f6c1"
	},
	"id": 18860,
	"nombre": "VALPARAÍSO",
	"idDepartamento": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f773"
	},
	"id": 5147,
	"nombre": "CAREPA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f854"
	},
	"id": 25572,
	"nombre": "PUERTO SALGAR",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f8b8"
	},
	"id": 68013,
	"nombre": "AGUADA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f73e"
	},
	"id": 68522,
	"nombre": "PALMAR",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f7d2"
	},
	"id": 47030,
	"nombre": "ALGARROBO",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f89a"
	},
	"id": 5315,
	"nombre": "GUADALUPE",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f949"
	},
	"id": 15362,
	"nombre": "IZA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f994"
	},
	"id": 47170,
	"nombre": "CHIVOLO",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa5c"
	},
	"id": 54418,
	"nombre": "LOURDES",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa75"
	},
	"id": 68266,
	"nombre": "ENCISO",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8faa7"
	},
	"id": 15820,
	"nombre": "TOPAGA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f8d1"
	},
	"id": 15696,
	"nombre": "SANTA SOFÍA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f903"
	},
	"id": 25295,
	"nombre": "GACHANCIPÁ",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f935"
	},
	"id": 17446,
	"nombre": "MARULANDA",
	"idDepartamento": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f94e"
	},
	"id": 15226,
	"nombre": "CUÍTIVA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f967"
	},
	"id": 70508,
	"nombre": "OVEJAS",
	"idDepartamento": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f9e4"
	},
	"id": 27245,
	"nombre": "EL CARMEN DE ATRATO",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa16"
	},
	"id": 23079,
	"nombre": "BUENAVISTA",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa2f"
	},
	"id": 20175,
	"nombre": "CHIMICHAGUA",
	"idDepartamento": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f31e"
	},
	"id": 5647,
	"nombre": "SAN ANDRÉS",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa7a"
	},
	"id": 54109,
	"nombre": "BUCARASICA",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f463"
	},
	"id": 68079,
	"nombre": "BARICHARA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f47c"
	},
	"id": 13440,
	"nombre": "MARGARITA",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f577"
	},
	"id": 15759,
	"nombre": "SOGAMOSO",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5db"
	},
	"id": 73024,
	"nombre": "ALPUJARRA",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f60d"
	},
	"id": 52240,
	"nombre": "CHACHAGUÍ",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f626"
	},
	"id": 25506,
	"nombre": "VENECIA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f314"
	},
	"id": 25592,
	"nombre": "QUEBRADANEGRA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f346"
	},
	"id": 5659,
	"nombre": "SAN JUAN DE URABÁ",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3aa"
	},
	"id": 27050,
	"nombre": "ATRATO (YUTO)",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3c3"
	},
	"id": 25293,
	"nombre": "GACHALA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3dc"
	},
	"id": 5044,
	"nombre": "ANZÁ",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f40e"
	},
	"id": 15380,
	"nombre": "LA CAPILLA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f752"
	},
	"id": 76670,
	"nombre": "SAN PEDRO",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f459"
	},
	"id": 8520,
	"nombre": "PALMAR DE VARELA",
	"idDepartamento": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f472"
	},
	"id": 68190,
	"nombre": "CIMITARRA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f79b"
	},
	"id": 5148,
	"nombre": "CARMEN DE VIBORAL",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f508"
	},
	"id": 27150,
	"nombre": "CARMEN DEL DARIEN",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f831"
	},
	"id": 25513,
	"nombre": "PACHO",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f53a"
	},
	"id": 68318,
	"nombre": "GUACA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f56d"
	},
	"id": 15720,
	"nombre": "SATIVANORTE",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5d1"
	},
	"id": 52885,
	"nombre": "YACUANQUER",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f8f9"
	},
	"id": 25126,
	"nombre": "CAJICÁ",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f92b"
	},
	"id": 15104,
	"nombre": "BOYACÁ",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f95d"
	},
	"id": 15407,
	"nombre": "VILLA DE LEYVA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f667"
	},
	"id": 19300,
	"nombre": "GUACHENE",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f98f"
	},
	"id": 13810,
	"nombre": "TIQUISIO",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f9a8"
	},
	"id": 44650,
	"nombre": "SAN JUAN DEL CESAR",
	"idDepartamento": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f9c1"
	},
	"id": 13683,
	"nombre": "SANTA ROSA",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f9f3"
	},
	"id": 76497,
	"nombre": "OBANDO",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa0c"
	},
	"id": 23686,
	"nombre": "SAN PELAYO",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa57"
	},
	"id": 44430,
	"nombre": "MAICAO",
	"idDepartamento": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa70"
	},
	"id": 54172,
	"nombre": "CHINÁCOTA",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2ce"
	},
	"id": 68322,
	"nombre": "GUAPOTÁ",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2e7"
	},
	"id": 63190,
	"nombre": "CIRCASIA",
	"idDepartamento": 63,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fabb"
	},
	"id": 20045,
	"nombre": "BECERRIL",
	"idDepartamento": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f364"
	},
	"id": 5347,
	"nombre": "HELICONIA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f396"
	},
	"id": 15816,
	"nombre": "TOGÜÍ",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f413"
	},
	"id": 15189,
	"nombre": "CIÉNEGA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f8a4"
	},
	"id": 5364,
	"nombre": "JARDÍN",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f908"
	},
	"id": 25224,
	"nombre": "CUCUNUBÁ",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4db"
	},
	"id": 23464,
	"nombre": "MOMIL",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f99e"
	},
	"id": 68575,
	"nombre": "PUERTO WILCHES",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5bd"
	},
	"id": 76130,
	"nombre": "CANDELARIA",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5ef"
	},
	"id": 52786,
	"nombre": "TAMINANGO",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f685"
	},
	"id": 41797,
	"nombre": "TESALIA",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f71b"
	},
	"id": 97161,
	"nombre": "CARURÚ",
	"idDepartamento": 97,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f734"
	},
	"id": 68855,
	"nombre": "VALLE DE SAN JOSÉ",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f7af"
	},
	"id": 73547,
	"nombre": "PIEDRAS",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f9ee"
	},
	"id": 15425,
	"nombre": "MACANAL",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa52"
	},
	"id": 44874,
	"nombre": "VILLANUEVA",
	"idDepartamento": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fab6"
	},
	"id": 91407,
	"nombre": "LA PEDRERA",
	"idDepartamento": 91,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fb5c"
	},
	"id": 50287,
	"nombre": "FUENTE DE ORO",
	"idDepartamento": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fc3d"
	},
	"id": 52427,
	"nombre": "MAGUI",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fc88"
	},
	"id": 68679,
	"nombre": "SAN GIL",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fca1"
	},
	"id": 68770,
	"nombre": "SUAITA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fcba"
	},
	"id": 73347,
	"nombre": "HERVEO",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe18"
	},
	"id": 68081,
	"nombre": "BARRANCABERMEJA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe31"
	},
	"id": 25320,
	"nombre": "GUADUAS",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe7c"
	},
	"id": 5091,
	"nombre": "BETANIA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe95"
	},
	"id": 15051,
	"nombre": "ARCABUCO",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ff44"
	},
	"id": 70400,
	"nombre": "LA UNION",
	"idDepartamento": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ff8f"
	},
	"id": 54520,
	"nombre": "PAMPLONITA",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ffd6"
	},
	"id": 68705,
	"nombre": "SANTA BÁRBARA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ffef"
	},
	"id": 19392,
	"nombre": "LA SIERRA",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8faee"
	},
	"id": 76869,
	"nombre": "VIJES",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fb07"
	},
	"id": 73563,
	"nombre": "PRADO",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fc33"
	},
	"id": 52560,
	"nombre": "POTOSI",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fcc9"
	},
	"id": 73461,
	"nombre": "MURILLO",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fcfb"
	},
	"id": 13473,
	"nombre": "MORALES",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fddc"
	},
	"id": 5284,
	"nombre": "FRONTINO",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe8b"
	},
	"id": 15238,
	"nombre": "DUITAMA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fed6"
	},
	"id": 8137,
	"nombre": "CAMPO DE LA CRUZ",
	"idDepartamento": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ff08"
	},
	"id": 27413,
	"nombre": "LLORO",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ff9e"
	},
	"id": 15180,
	"nombre": "CHISCAS",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb901a7"
	},
	"id": 68861,
	"nombre": "VÉLEZ",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fc79"
	},
	"id": 50689,
	"nombre": "SAN MARTIN",
	"idDepartamento": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fd41"
	},
	"id": 68296,
	"nombre": "GALÁN",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fdd7"
	},
	"id": 5264,
	"nombre": "ENTRERRIOS",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe54"
	},
	"id": 25377,
	"nombre": "LA CALERA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe86"
	},
	"id": 15232,
	"nombre": "CHÍQUIZA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ff67"
	},
	"id": 68502,
	"nombre": "ONZAGA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ffb2"
	},
	"id": 15223,
	"nombre": "CUBARÁ",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb90157"
	},
	"id": 86749,
	"nombre": "SIBUNDOY",
	"idDepartamento": 86,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8faf3"
	},
	"id": 76364,
	"nombre": "JAMUNDÍ",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fb25"
	},
	"id": 52506,
	"nombre": "OSPINA",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fce7"
	},
	"id": 73270,
	"nombre": "FALAN",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fd19"
	},
	"id": 68176,
	"nombre": "CHIMA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fd32"
	},
	"id": 68444,
	"nombre": "MATANZA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fd64"
	},
	"id": 25430,
	"nombre": "MADRID",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fdfa"
	},
	"id": 5400,
	"nombre": "LA UNIÓN",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe13"
	},
	"id": 15761,
	"nombre": "SOMONDOCO",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe5e"
	},
	"id": 5002,
	"nombre": "ABEJORRAL",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fc74"
	},
	"id": 95200,
	"nombre": "MIRAFLORES",
	"idDepartamento": 95,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe1d"
	},
	"id": 25168,
	"nombre": "CHAGUANÍ",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe4f"
	},
	"id": 17050,
	"nombre": "ARANZAZU",
	"idDepartamento": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe81"
	},
	"id": 15131,
	"nombre": "CALDAS",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fecc"
	},
	"id": 8638,
	"nombre": "SABANALARGA",
	"idDepartamento": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ffc6"
	},
	"id": 15518,
	"nombre": "PAJARITO",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ffdb"
	},
	"id": 15183,
	"nombre": "CHITA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fff4"
	},
	"id": 44110,
	"nombre": "EL MOLINO",
	"idDepartamento": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb902b6"
	},
	"id": 25743,
	"nombre": "SILVANIA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb902cf"
	},
	"id": 5607,
	"nombre": "RETIRO",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb9040a"
	},
	"id": 47570,
	"nombre": "PUEBLOVIEJO",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90482"
	},
	"id": 23350,
	"nombre": "LA APARTADA",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb90365"
	},
	"id": 25019,
	"nombre": "ALBÁN",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90504"
	},
	"id": 15087,
	"nombre": "BELÉN",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb905e4"
	},
	"id": 50370,
	"nombre": "URIBE",
	"idDepartamento": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90643"
	},
	"id": 41013,
	"nombre": "AGRADO",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb903fb"
	},
	"id": 27135,
	"nombre": "EL CANTÓN DEL SAN PABLO",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9067a"
	},
	"id": 25339,
	"nombre": "GUTIÉRREZ",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9050e"
	},
	"id": 44378,
	"nombre": "HATO NUEVO",
	"idDepartamento": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb905bc"
	},
	"id": 52720,
	"nombre": "SAPUYES",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90729"
	},
	"id": 54670,
	"nombre": "SAN CALIXTO",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90701"
	},
	"id": 68755,
	"nombre": "SOCORRO",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90733"
	},
	"id": 54498,
	"nombre": "OCAÑA",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9074c"
	},
	"id": 70742,
	"nombre": "SINCÉ",
	"idDepartamento": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90887"
	},
	"id": 5585,
	"nombre": "PUERTO NARE",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb908a0"
	},
	"id": 15600,
	"nombre": "RÁQUIRA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb908b9"
	},
	"id": 25151,
	"nombre": "CAQUEZA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb907c9"
	},
	"id": 63111,
	"nombre": "BUENAVISTA",
	"idDepartamento": 63,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90814"
	},
	"id": 25658,
	"nombre": "SAN FRANCISCO",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90927"
	},
	"id": 15001,
	"nombre": "TUNJA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90882"
	},
	"id": 27800,
	"nombre": "UNGUÍA",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9089b"
	},
	"id": 15599,
	"nombre": "RAMIRIQUÍ",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb908b4"
	},
	"id": 15580,
	"nombre": "QUÍPAMA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90931"
	},
	"id": 5342,
	"nombre": "CAMPAMENTO",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb904d7"
	},
	"id": 15723,
	"nombre": "SATIVASUR",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90509"
	},
	"id": 76828,
	"nombre": "TRUJILLO",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90747"
	},
	"id": 5113,
	"nombre": "BURITICÁ",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb908be"
	},
	"id": 15879,
	"nombre": "VIRACACHÁ",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb90306"
	},
	"id": 5809,
	"nombre": "TITIRIBÍ",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb904e1"
	},
	"id": 54125,
	"nombre": "CÁCOTA",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90738"
	},
	"id": 5138,
	"nombre": "CAÑASGORDAS",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90751"
	},
	"id": 73349,
	"nombre": "HONDA",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb907ce"
	},
	"id": 68255,
	"nombre": "EL PLAYÓN",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90819"
	},
	"id": 25580,
	"nombre": "PULÍ",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90896"
	},
	"id": 15690,
	"nombre": "SANTA MARÍA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb908af"
	},
	"id": 15740,
	"nombre": "SIACHOQUE",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb908fa"
	},
	"id": 17486,
	"nombre": "NEIRA",
	"idDepartamento": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90913"
	},
	"id": 15507,
	"nombre": "OTANCHE",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9095e"
	},
	"id": 8560,
	"nombre": "PONEDERA",
	"idDepartamento": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb909a9"
	},
	"id": 13248,
	"nombre": "EL GUAMO",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb909dd"
	},
	"id": 15798,
	"nombre": "TENZA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb909e7"
	},
	"id": 23189,
	"nombre": "CIÉNAGA DE ORO",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb909c9"
	},
	"id": 13030,
	"nombre": "ALTOS DEL ROSARIO",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90a8c"
	},
	"id": 91540,
	"nombre": "PUERTO NARIÑO",
	"idDepartamento": 91,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90a1e"
	},
	"id": 44098,
	"nombre": "DISTRACCIÓN",
	"idDepartamento": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90a69"
	},
	"id": 15533,
	"nombre": "PAYA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edf3"
	},
	"id": 66001,
	"nombre": "PEREIRA",
	"idDepartamento": 66,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee25"
	},
	"id": 5819,
	"nombre": "TOLEDO",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edf8"
	},
	"id": 25871,
	"nombre": "VILLAGÓMEZ",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee5c"
	},
	"id": 5237,
	"nombre": "DON MATÍAS",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee75"
	},
	"id": 5576,
	"nombre": "PUEBLORRICO",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef51"
	},
	"id": 13838,
	"nombre": "TURBANA",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efe7"
	},
	"id": 85440,
	"nombre": "VILLANUEVA",
	"idDepartamento": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f019"
	},
	"id": 54239,
	"nombre": "DURANIA",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f04f"
	},
	"id": 76100,
	"nombre": "BOLÍVAR",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ec72"
	},
	"id": 25053,
	"nombre": "ARBELÁEZ",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ecbd"
	},
	"id": 19548,
	"nombre": "PIENDAMÓ",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee02"
	},
	"id": 25491,
	"nombre": "NOCAIMA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee1b"
	},
	"id": 25807,
	"nombre": "TIBIRITA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef60"
	},
	"id": 8436,
	"nombre": "MANATI",
	"idDepartamento": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef79"
	},
	"id": 13673,
	"nombre": "SANTA CATALINA",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f041"
	},
	"id": 15248,
	"nombre": "EL ESPINO",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f072"
	},
	"id": 15822,
	"nombre": "TOTA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed8a"
	},
	"id": 47605,
	"nombre": "REMOLINO",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8edbc"
	},
	"id": 68368,
	"nombre": "JESÚS MARÍA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee6b"
	},
	"id": 5266,
	"nombre": "ENVIGADO",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eee8"
	},
	"id": 17616,
	"nombre": "RISARALDA",
	"idDepartamento": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef4c"
	},
	"id": 8078,
	"nombre": "BARANOA",
	"idDepartamento": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f077"
	},
	"id": 15276,
	"nombre": "FLORESTA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ebbe"
	},
	"id": 52210,
	"nombre": "CONTADERO",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eeac"
	},
	"id": 15897,
	"nombre": "ZETAQUIRA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8eec5"
	},
	"id": 25281,
	"nombre": "FOSCA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef29"
	},
	"id": 15022,
	"nombre": "ALMEIDA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef5b"
	},
	"id": 13442,
	"nombre": "MARIA LA BAJA",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f0d1"
	},
	"id": 52001,
	"nombre": "PASTO",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f211"
	},
	"id": 97511,
	"nombre": "PACOA (COR. DEPARTAMENTAL)",
	"idDepartamento": 97,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f29d"
	},
	"id": 47545,
	"nombre": "PIJIÑO  DEL CARMEN",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f1fd"
	},
	"id": 68720,
	"nombre": "SANTA HELENA DEL OPÓN",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f234"
	},
	"id": 5142,
	"nombre": "CARACOLÍ",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3c9"
	},
	"id": 25328,
	"nombre": "GUAYABAL DE SÍQUIMA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3fb"
	},
	"id": 5088,
	"nombre": "BELLO",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4dc"
	},
	"id": 8832,
	"nombre": "TUBARÁ",
	"idDepartamento": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f7e7"
	},
	"id": 68179,
	"nombre": "CHIPATÁ",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f800"
	},
	"id": 68167,
	"nombre": "CHARALÁ",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f864"
	},
	"id": 25805,
	"nombre": "TIBACUY",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f990"
	},
	"id": 8001,
	"nombre": "BARRANQUILLA",
	"idDepartamento": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f310"
	},
	"id": 25662,
	"nombre": "SAN JUAN DE RÍO SECO",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f423"
	},
	"id": 17665,
	"nombre": "SAN JOSÉ",
	"idDepartamento": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4a0"
	},
	"id": 27361,
	"nombre": "ISTMINA",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f582"
	},
	"id": 99524,
	"nombre": "LA PRIMAVERA",
	"idDepartamento": 99,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa3f"
	},
	"id": 68682,
	"nombre": "SAN JOAQUÍN",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa71"
	},
	"id": 44035,
	"nombre": "ALBANIA",
	"idDepartamento": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f85a"
	},
	"id": 5615,
	"nombre": "RIONEGRO",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f8a5"
	},
	"id": 5306,
	"nombre": "GIRALDO",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f8d7"
	},
	"id": 15646,
	"nombre": "SAMACÁ",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f36a"
	},
	"id": 5310,
	"nombre": "GÓMEZ PLATA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f383"
	},
	"id": 5495,
	"nombre": "NECHÍ",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f400"
	},
	"id": 5059,
	"nombre": "ARMENIA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f513"
	},
	"id": 68686,
	"nombre": "SAN MIGUEL",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f60e"
	},
	"id": 52227,
	"nombre": "CUMBAL",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f672"
	},
	"id": 41244,
	"nombre": "ELÍAS",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f81e"
	},
	"id": 63401,
	"nombre": "LA TEBAIDA",
	"idDepartamento": 63,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f837"
	},
	"id": 25898,
	"nombre": "ZIPACÓN",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f8b4"
	},
	"id": 5282,
	"nombre": "FREDONIA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f995"
	},
	"id": 47798,
	"nombre": "TENERIFE",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa76"
	},
	"id": 68207,
	"nombre": "CONCEPCIÓN",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa8f"
	},
	"id": 15537,
	"nombre": "PAZ DE RÍO",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4ff"
	},
	"id": 85136,
	"nombre": "LA SALINA",
	"idDepartamento": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f564"
	},
	"id": 25290,
	"nombre": "FUSAGASUGÁ",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f57d"
	},
	"id": 50245,
	"nombre": "EL CALVARIO",
	"idDepartamento": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f613"
	},
	"id": 52385,
	"nombre": "LA LLANADA",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f774"
	},
	"id": 5150,
	"nombre": "CAROLINA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f7bf"
	},
	"id": 23162,
	"nombre": "CERETÉ",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f7d8"
	},
	"id": 23672,
	"nombre": "SAN ANTERO",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f8a0"
	},
	"id": 5353,
	"nombre": "HISPANIA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f9b3"
	},
	"id": 13620,
	"nombre": "SAN CRISTOBAL",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa49"
	},
	"id": 20060,
	"nombre": "BOSCONIA",
	"idDepartamento": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2fc"
	},
	"id": 25899,
	"nombre": "ZIPAQUIRÁ",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f32e"
	},
	"id": 25653,
	"nombre": "SAN CAYETANO",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4d7"
	},
	"id": 20295,
	"nombre": "GAMARRA",
	"idDepartamento": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f8dc"
	},
	"id": 5101,
	"nombre": "CIUDAD BOLÍVAR",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f8f5"
	},
	"id": 25001,
	"nombre": "AGUA DE DIOS",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f90e"
	},
	"id": 25288,
	"nombre": "FÚQUENE",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fd06"
	},
	"id": 47205,
	"nombre": "CONCORDIA",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe19"
	},
	"id": 15638,
	"nombre": "SÁCHICA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ffc2"
	},
	"id": 15522,
	"nombre": "PANQUEBA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fc5c"
	},
	"id": 52473,
	"nombre": "MOSQUERA",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fd88"
	},
	"id": 25781,
	"nombre": "SUTATAUSA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe1e"
	},
	"id": 25307,
	"nombre": "GIRARDOT",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ff18"
	},
	"id": 13188,
	"nombre": "CICUCO",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fffa"
	},
	"id": 20550,
	"nombre": "PELAYA",
	"idDepartamento": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe91"
	},
	"id": 15185,
	"nombre": "CHITARAQUE",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8feaa"
	},
	"id": 15236,
	"nombre": "CHIVOR",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ff40"
	},
	"id": 70702,
	"nombre": "SAN JUAN BETULIA",
	"idDepartamento": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ffbd"
	},
	"id": 15244,
	"nombre": "EL COCUY",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb901b2"
	},
	"id": 68549,
	"nombre": "PINCHOTE",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fc48"
	},
	"id": 52699,
	"nombre": "SANTACRUZ",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fd42"
	},
	"id": 68209,
	"nombre": "CONFINES",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ff1d"
	},
	"id": 17388,
	"nombre": "LA MERCED",
	"idDepartamento": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fd2e"
	},
	"id": 68169,
	"nombre": "CHARTA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe73"
	},
	"id": 17524,
	"nombre": "PALESTINA",
	"idDepartamento": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ff09"
	},
	"id": 13140,
	"nombre": "CALAMAR",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb90230"
	},
	"id": 13667,
	"nombre": "SAN MARTIN DE LOBA",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb903c0"
	},
	"id": 15476,
	"nombre": "MOTAVITA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb904a1"
	},
	"id": 85315,
	"nombre": "SÁCAMA",
	"idDepartamento": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90519"
	},
	"id": 44560,
	"nombre": "MANAURE",
	"idDepartamento": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90523"
	},
	"id": 85015,
	"nombre": "CHÁMEZA",
	"idDepartamento": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90649"
	},
	"id": 41026,
	"nombre": "ALTAMIRA",
	"idDepartamento": 41,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb906ad"
	},
	"id": 86219,
	"nombre": "COLON",
	"idDepartamento": 86,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90716"
	},
	"id": 97777,
	"nombre": "PAPUNAUA (COR. DEPARTAMENTAL)",
	"idDepartamento": 97,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90748"
	},
	"id": 76823,
	"nombre": "TORO",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90824"
	},
	"id": 50226,
	"nombre": "CUMARAL",
	"idDepartamento": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb907de"
	},
	"id": 25867,
	"nombre": "VIANÍ",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90874"
	},
	"id": 5321,
	"nombre": "GUATAPÉ",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb908f1"
	},
	"id": 25312,
	"nombre": "GRANADA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb904dd"
	},
	"id": 15839,
	"nombre": "TUTAZÁ",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9058b"
	},
	"id": 76111,
	"nombre": "BUGA",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb905d6"
	},
	"id": 52083,
	"nombre": "BELÉN",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9082e"
	},
	"id": 25843,
	"nombre": "VILLA DE SAN DIEGO DE UBATE",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb908ab"
	},
	"id": 15778,
	"nombre": "SUTATENZA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9081a"
	},
	"id": 25612,
	"nombre": "RICAURTE",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb908b0"
	},
	"id": 27810,
	"nombre": "UNIÓN PANAMERICANA ( ANIMAS)",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9095f"
	},
	"id": 20001,
	"nombre": "VALLEDUPAR",
	"idDepartamento": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90a3d"
	},
	"id": 44001,
	"nombre": "RIOHACHA",
	"idDepartamento": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9096e"
	},
	"id": 8675,
	"nombre": "SANTA LUCIA",
	"idDepartamento": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90a8d"
	},
	"id": 20621,
	"nombre": "LA PAZ",
	"idDepartamento": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90a83"
	},
	"id": 19807,
	"nombre": "TIMBÍO",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90b77"
	},
	"id": 19780,
	"nombre": "SUÁREZ",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90b4f"
	},
	"id": 25524,
	"nombre": "PANDI",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90c71"
	},
	"id": 97889,
	"nombre": "YABARATÉ",
	"idDepartamento": 97,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90ca3"
	},
	"id": 70820,
	"nombre": "TOLÚ",
	"idDepartamento": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90d25"
	},
	"id": 68320,
	"nombre": "GUADALUPE",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90d9d"
	},
	"id": 5658,
	"nombre": "SAN JOSÉ DE LA MONTAÑA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90dcf"
	},
	"id": 5861,
	"nombre": "VENECIA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90faa"
	},
	"id": 15332,
	"nombre": "GÜICÁN",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb90fdd"
	},
	"id": 27250,
	"nombre": "EL LITORAL DEL SAN JUAN",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb91186"
	},
	"id": 52520,
	"nombre": "FRANCISCO PIZARRO",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb9148e"
	},
	"id": 73055,
	"nombre": "ARMERO",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90e79"
	},
	"id": 15176,
	"nombre": "CHIQUINQUIRÁ",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90f8c"
	},
	"id": 15757,
	"nombre": "SOCHA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90dde"
	},
	"id": 5591,
	"nombre": "PUERTO TRIUNFO",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb911ae"
	},
	"id": 94883,
	"nombre": "SAN FELIPE",
	"idDepartamento": 94,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb9146b"
	},
	"id": 23500,
	"nombre": "MOÑITOS",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb91524"
	},
	"id": 91798,
	"nombre": "TARAPACÁ",
	"idDepartamento": 91,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb916af"
	},
	"id": 52287,
	"nombre": "FUNES",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb9160a"
	},
	"id": 19513,
	"nombre": "PADILLA",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb9187b"
	},
	"id": 15762,
	"nombre": "SORA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb9188a"
	},
	"id": 15660,
	"nombre": "SAN EDUARDO",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb91952"
	},
	"id": 47245,
	"nombre": "EL BANCO",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb91709"
	},
	"id": 70823,
	"nombre": "TOLUVIEJO",
	"idDepartamento": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb919c3"
	},
	"id": 15776,
	"nombre": "SUTAMARCHÁN",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90cc6"
	},
	"id": 73030,
	"nombre": "AMBALEMA",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90f37"
	},
	"id": 23586,
	"nombre": "PURISIMA",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb90fce"
	},
	"id": 85279,
	"nombre": "RECETOR",
	"idDepartamento": 85,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91a6d"
	},
	"id": 25817,
	"nombre": "TOCANCIPÁ",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91a72"
	},
	"id": 44420,
	"nombre": "LA JAGUA DEL PILAR",
	"idDepartamento": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91ca8"
	},
	"id": 23168,
	"nombre": "CHIMA",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91e88"
	},
	"id": 8758,
	"nombre": "SOLEDAD",
	"idDepartamento": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91eec"
	},
	"id": 27073,
	"nombre": "BAGADO",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee8a"
	},
	"id": 15804,
	"nombre": "TIBANÁ",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ee7b"
	},
	"id": 5475,
	"nombre": "MURINDÓ",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f06e"
	},
	"id": 91536,
	"nombre": "PUERTO ARICA",
	"idDepartamento": 91,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efc5"
	},
	"id": 70418,
	"nombre": "LOS PALMITOS",
	"idDepartamento": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed0e"
	},
	"id": 70124,
	"nombre": "CAIMITO",
	"idDepartamento": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed72"
	},
	"id": 20238,
	"nombre": "EL COPEY",
	"idDepartamento": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ed8b"
	},
	"id": 44090,
	"nombre": "DIBULLA",
	"idDepartamento": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8ef4d"
	},
	"id": 47541,
	"nombre": "PEDRAZA",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8efca"
	},
	"id": 70110,
	"nombre": "BUENAVISTA",
	"idDepartamento": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f02e"
	},
	"id": 54347,
	"nombre": "HERRÁN",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f078"
	},
	"id": 5642,
	"nombre": "SALGAR",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f082"
	},
	"id": 76306,
	"nombre": "GINEBRA",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f091"
	},
	"id": 73854,
	"nombre": "VALLE DE SAN JUAN",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f21c"
	},
	"id": 70523,
	"nombre": "PALMITO",
	"idDepartamento": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f2b2"
	},
	"id": 68217,
	"nombre": "COROMORO",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f26c"
	},
	"id": 73152,
	"nombre": "CASABIANCA",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f415"
	},
	"id": 15500,
	"nombre": "OICATÁ",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f460"
	},
	"id": 47980,
	"nombre": "ZONA BANANERA",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f574"
	},
	"id": 15377,
	"nombre": "LABRANZAGRANDE",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f833"
	},
	"id": 25875,
	"nombre": "VILLETA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f8e2"
	},
	"id": 15676,
	"nombre": "SAN MIGUEL DE SEMA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f946"
	},
	"id": 15494,
	"nombre": "NUEVO COLÓN",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f991"
	},
	"id": 8558,
	"nombre": "POLONUEVO",
	"idDepartamento": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f4e2"
	},
	"id": 23678,
	"nombre": "SAN CARLOS",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f86a"
	},
	"id": 25799,
	"nombre": "TENJO",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa13"
	},
	"id": 23670,
	"nombre": "SAN ANDRES DE SOTAVENTO",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f30c"
	},
	"id": 25745,
	"nombre": "SIMIJACA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f41f"
	},
	"id": 17877,
	"nombre": "VITERBO",
	"idDepartamento": 17,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f678"
	},
	"id": 19845,
	"nombre": "VILLA RICA",
	"idDepartamento": 19,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f775"
	},
	"id": 76020,
	"nombre": "ALCALÁ",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f410"
	},
	"id": 15224,
	"nombre": "CUCAITA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f32a"
	},
	"id": 25797,
	"nombre": "TENA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa54"
	},
	"id": 44279,
	"nombre": "FONSECA",
	"idDepartamento": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f46f"
	},
	"id": 47703,
	"nombre": "SAN ZENÓN",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f8f1"
	},
	"id": 25099,
	"nombre": "BOJACÁ",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa81"
	},
	"id": 15215,
	"nombre": "CORRALES",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe65"
	},
	"id": 15106,
	"nombre": "BRICEÑO",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fc3a"
	},
	"id": 52683,
	"nombre": "SANDONÁ",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fff1"
	},
	"id": 27491,
	"nombre": "NÓVITA",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb902a4"
	},
	"id": 25483,
	"nombre": "NARIÑO",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb90407"
	},
	"id": 47161,
	"nombre": "CERRO DE SAN ANTONIO",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90587"
	},
	"id": 76248,
	"nombre": "EL CERRITO",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90816"
	},
	"id": 25754,
	"nombre": "SOACHA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb906ef"
	},
	"id": 97001,
	"nombre": "MITU",
	"idDepartamento": 97,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb907ee"
	},
	"id": 63470,
	"nombre": "MONTENEGRO",
	"idDepartamento": 63,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9087f"
	},
	"id": 5501,
	"nombre": "OLAYA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90965"
	},
	"id": 47288,
	"nombre": "FUNDACIÓN",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9096a"
	},
	"id": 47707,
	"nombre": "SANTA ANA",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9097e"
	},
	"id": 13647,
	"nombre": "SAN ESTANISLAO",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90960"
	},
	"id": 47660,
	"nombre": "SABANAS DE SAN ÁNGEL",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90c63"
	},
	"id": 95015,
	"nombre": "CALAMAR",
	"idDepartamento": 95,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90dfd"
	},
	"id": 68051,
	"nombre": "ARATOCA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90f1f"
	},
	"id": 13062,
	"nombre": "ARROYOHONDO",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb90fe3"
	},
	"id": 76054,
	"nombre": "ARGELIA",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb90fcf"
	},
	"id": 15466,
	"nombre": "MONGUI",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90f88"
	},
	"id": 68162,
	"nombre": "CERRITO",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb90fd9"
	},
	"id": 91405,
	"nombre": "LA CHORRERA",
	"idDepartamento": 91,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb91282"
	},
	"id": 63690,
	"nombre": "SALENTO",
	"idDepartamento": 63,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb916c4"
	},
	"id": 52320,
	"nombre": "GUAITARILLA",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb916ce"
	},
	"id": 73148,
	"nombre": "CARMEN APICALA",
	"idDepartamento": 73,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb91782"
	},
	"id": 47001,
	"nombre": "SANTA MARTA",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb918fe"
	},
	"id": 15162,
	"nombre": "CERINZA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb9193f"
	},
	"id": 20013,
	"nombre": "AGUSTIN CODAZZI",
	"idDepartamento": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb91908"
	},
	"id": 15401,
	"nombre": "LA VICTORIA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91a1e"
	},
	"id": 68425,
	"nombre": "MACARAVITA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb919c4"
	},
	"id": 23300,
	"nombre": "COTORRA",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91a37"
	},
	"id": 15464,
	"nombre": "MONGUA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91a73"
	},
	"id": 91669,
	"nombre": "PUERTO SANTANDER",
	"idDepartamento": 91,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91ae7"
	},
	"id": 52418,
	"nombre": "LOS ANDES (SOTOMAYOR)",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91c45"
	},
	"id": 94888,
	"nombre": "PANÁ PANÁ",
	"idDepartamento": 94,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91c4f"
	},
	"id": 70265,
	"nombre": "GUARANDA",
	"idDepartamento": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91de9"
	},
	"id": 25200,
	"nombre": "COGUA",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91e84"
	},
	"id": 8433,
	"nombre": "MALAMBO",
	"idDepartamento": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fb6965613a0cb92425"
	},
	"id": 76863,
	"nombre": "VERSALLES",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fc6965613a0cb92a17"
	},
	"id": 99001,
	"nombre": "PUERTO CARREÑO",
	"idDepartamento": 99,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fb6965613a0cb92475"
	},
	"id": 54553,
	"nombre": "PUERTO SANTANDER",
	"idDepartamento": 54,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fc6965613a0cb928f5"
	},
	"id": 8770,
	"nombre": "SUAN",
	"idDepartamento": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fc6965613a0cb929f9"
	},
	"id": 15092,
	"nombre": "BETEITIVA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fc6965613a0cb92c46"
	},
	"id": 13074,
	"nombre": "BOLÍVAR",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fc6965613a0cb92caa"
	},
	"id": 68327,
	"nombre": "GÜEPSA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fc6965613a0cb92e62"
	},
	"id": 13580,
	"nombre": "REGIDOR",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fc6965613a0cb92d9f"
	},
	"id": 25324,
	"nombre": "GUATAQUÍ",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fc6965613a0cb92d95"
	},
	"id": 25086,
	"nombre": "BELTRÁN",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fc6965613a0cb92e44"
	},
	"id": 8606,
	"nombre": "REPELON",
	"idDepartamento": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fd6965613a0cb934a7"
	},
	"id": 91530,
	"nombre": "PUERTO ALEGRÍA",
	"idDepartamento": 91,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f06a"
	},
	"id": 15403,
	"nombre": "LA UVITA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ee6965613a0cb8f079"
	},
	"id": 88001,
	"nombre": "SAN ANDRES",
	"idDepartamento": 88,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f466"
	},
	"id": 8141,
	"nombre": "CANDELARIA",
	"idDepartamento": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f52e"
	},
	"id": 15114,
	"nombre": "BUSBANZÁ",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f556"
	},
	"id": 15810,
	"nombre": "TIPACOQUE",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f3a3"
	},
	"id": 50686,
	"nombre": "SAN JUANITO",
	"idDepartamento": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f45c"
	},
	"id": 8849,
	"nombre": "USIACURI",
	"idDepartamento": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f750"
	},
	"id": 94886,
	"nombre": "CACAHUAL",
	"idDepartamento": 94,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f385"
	},
	"id": 5390,
	"nombre": "LA PINTADA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f9bf"
	},
	"id": 13760,
	"nombre": "SOPLAVIENTO",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fcef"
	},
	"id": 47720,
	"nombre": "SANTA BÁRBARA DE PINTO",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fe25"
	},
	"id": 25269,
	"nombre": "FACATATIVÁ",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb905c4"
	},
	"id": 52480,
	"nombre": "NARIÑO",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ff4c"
	},
	"id": 23675,
	"nombre": "SAN BERNARDO DEL VIENTO",
	"idDepartamento": 23,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9078b"
	},
	"id": 20787,
	"nombre": "TAMALAMEQUE",
	"idDepartamento": 20,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8ffba"
	},
	"id": 15296,
	"nombre": "GAMEZA",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb901ec"
	},
	"id": 70221,
	"nombre": "COVEÑAS",
	"idDepartamento": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90534"
	},
	"id": 68132,
	"nombre": "CALIFORNIA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90c28"
	},
	"id": 52256,
	"nombre": "EL ROSARIO",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fb6965613a0cb92711"
	},
	"id": 13600,
	"nombre": "RIOVIEJO",
	"idDepartamento": 13,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fb6965613a0cb92725"
	},
	"id": 47258,
	"nombre": "EL PIÑÓN",
	"idDepartamento": 47,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8faba"
	},
	"id": 63001,
	"nombre": "ARMENIA",
	"idDepartamento": 63,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8fa51"
	},
	"id": 44078,
	"nombre": "BARRANCAS",
	"idDepartamento": 44,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fd9f"
	},
	"id": 25785,
	"nombre": "TABIO",
	"idDepartamento": 25,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fd6965613a0cb933f9"
	},
	"id": 15187,
	"nombre": "CHIVATÁ",
	"idDepartamento": 15,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb8fcaa"
	},
	"id": 76147,
	"nombre": "CARTAGO",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90615"
	},
	"id": 50680,
	"nombre": "SAN CARLOS DE GUAROA",
	"idDepartamento": 50,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb9095d"
	},
	"id": 8634,
	"nombre": "SABANAGRANDE",
	"idDepartamento": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90a31"
	},
	"id": 68432,
	"nombre": "MÁLAGA",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb91518"
	},
	"id": 91263,
	"nombre": "EL ENCANTO",
	"idDepartamento": 91,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb91941"
	},
	"id": 8296,
	"nombre": "GALAPA",
	"idDepartamento": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91a66"
	},
	"id": 91430,
	"nombre": "LA VICTORIA",
	"idDepartamento": 91,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fb6965613a0cb9212f"
	},
	"id": 52565,
	"nombre": "PROVIDENCIA",
	"idDepartamento": 52,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fb6965613a0cb9216b"
	},
	"id": 94885,
	"nombre": "LA GUADALUPE",
	"idDepartamento": 94,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fd6965613a0cb9349a"
	},
	"id": 27745,
	"nombre": "SIPÍ",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fd6965613a0cb934a4"
	},
	"id": 27660,
	"nombre": "SAN JOSÉ DEL PALMAR",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04ff6965613a0cb93bda"
	},
	"id": 70235,
	"nombre": "GALERAS",
	"idDepartamento": 70,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf05006965613a0cb941b2"
	},
	"id": 68370,
	"nombre": "JORDÁN",
	"idDepartamento": 68,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf05006965613a0cb9463f"
	},
	"id": 97666,
	"nombre": "TARAIRA",
	"idDepartamento": 97,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf05016965613a0cb949ae"
	},
	"id": 27205,
	"nombre": "CONDOTO",
	"idDepartamento": 27,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f66965613a0cb90d79"
	},
	"id": 5631,
	"nombre": "SABANETA",
	"idDepartamento": 5,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb9116c"
	},
	"id": 18029,
	"nombre": "ALBANIA",
	"idDepartamento": 18,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb91776"
	},
	"id": 8685,
	"nombre": "SANTO TOMÁS",
	"idDepartamento": 8,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb91523"
	},
	"id": 88564,
	"nombre": "PROVIDENCIA",
	"idDepartamento": 88,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91fa9"
	},
	"id": 91001,
	"nombre": "LETICIA",
	"idDepartamento": 91,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fb6965613a0cb9216c"
	},
	"id": 94884,
	"nombre": "PUERTO COLOMBIA (COR. DEPARTAMENTAL)",
	"idDepartamento": 94,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf05006965613a0cb9446d"
	},
	"id": 76243,
	"nombre": "EL ÁGUILA",
	"idDepartamento": 76,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf05076965613a0cb963b0"
	},
	"id": 91460,
	"nombre": "MIRITÍ - PARANÁ",
	"idDepartamento": 91,
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf05086965613a0cb967b4"
	},
	"id": 8573,
	"nombre": "PUERTO COLOMBIA",
	"idDepartamento": 8,
	"__v": 0
}]

