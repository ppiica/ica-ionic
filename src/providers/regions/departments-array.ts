export const departmentsArray = [{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5a3"
	},
	"id": 68,
	"nombre": "SANTANDER",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f58f"
	},
	"id": 50,
	"nombre": "META",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5a8"
	},
	"id": 5,
	"nombre": "ANTIOQUIA",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f594"
	},
	"id": 73,
	"nombre": "TOLIMA",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f599"
	},
	"id": 52,
	"nombre": "NARIÑO",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5b2"
	},
	"id": 54,
	"nombre": "NORTE DE SANTANDER",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f595"
	},
	"id": 86,
	"nombre": "PUTUMAYO",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f59a"
	},
	"id": 94,
	"nombre": "GUAINIA",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5ae"
	},
	"id": 17,
	"nombre": "CALDAS",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5b3"
	},
	"id": 81,
	"nombre": "ARAUCA",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f16965613a0cb8fad9"
	},
	"id": 15,
	"nombre": "BOYACÁ",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f590"
	},
	"id": 41,
	"nombre": "HUILA",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5a4"
	},
	"id": 66,
	"nombre": "RISARALDA",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f16965613a0cb8fae3"
	},
	"id": 25,
	"nombre": "CUNDINAMARCA",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f16965613a0cb8facf"
	},
	"id": 13,
	"nombre": "BOLÍVAR",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb90021"
	},
	"id": 27,
	"nombre": "CHOCÓ",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb9001c"
	},
	"id": 23,
	"nombre": "CÓRDOBA",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90535"
	},
	"id": 76,
	"nombre": "VALLE DEL CAUCA",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90562"
	},
	"id": 19,
	"nombre": "CAUCA",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90567"
	},
	"id": 70,
	"nombre": "SUCRE",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90576"
	},
	"id": 20,
	"nombre": "CESAR",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f5b4"
	},
	"id": 11,
	"nombre": "BOGOTÁ, D.C.",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f06965613a0cb8f596"
	},
	"id": 18,
	"nombre": "CAQUETÁ",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f16965613a0cb8fad0"
	},
	"id": 63,
	"nombre": "QUINDIO",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f16965613a0cb8fae9"
	},
	"id": 47,
	"nombre": "MAGDALENA",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f16965613a0cb8fadf"
	},
	"id": 85,
	"nombre": "CASANARE",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f46965613a0cb90abb"
	},
	"id": 44,
	"nombre": "LA GUAJIRA",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91a96"
	},
	"id": 95,
	"nombre": "GUAVIARE",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f36965613a0cb9000a"
	},
	"id": 99,
	"nombre": "VICHADA",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f86965613a0cb91fbc"
	},
	"id": 97,
	"nombre": "VAUPES",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04f76965613a0cb9153d"
	},
	"id": 8,
	"nombre": "ATLÁNTICO",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf04fd6965613a0cb92f98"
	},
	"id": 91,
	"nombre": "AMAZONAS",
	"__v": 0
},
{
	"_id": {
		"$oid": "5aaf05066965613a0cb95e9c"
	},
	"id": 88,
	"nombre": "SAN ANDRES Y PROVIDENCIA",
	"__v": 0
}]

