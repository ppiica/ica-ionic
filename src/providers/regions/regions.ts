import { sidewalksArray } from './sidewalk-array';
import { IDepartment, IMunicipality, ISideWalk } from './../../models/regionsModel';
import { Injectable } from '@angular/core';
import * as _ from "lodash";
import { departmentsArray } from './departments-array';
import { municipalitiesArray } from './municipality-array';

@Injectable()
export class RegionsProvider {

  constructor() {
  }

  /**
   * getDepartments
   */
  public getDepartments(): Array<IDepartment> {
   return _.sortBy(departmentsArray, 'nombre');
  }

  /**
   * getMunicipalitiesByDepartmentId
   */
  public getMunicipalitiesByDepartmentId(departmentId: number): Array<IMunicipality> {
    return _.sortBy(_.filter(municipalitiesArray, { idDepartamento: departmentId }), 'nombre');
  }

  /**
   * getSidewalksByMunicipalityId
   */
  public getSidewalksByMunicipalityId(municipalityId: number): Array<ISideWalk> {
    return _.sortBy(_.filter(sidewalksArray, { idMunicipio: municipalityId }), 'nombre');
  }

}
