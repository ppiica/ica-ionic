import { ToastServiceProvider } from './../toast-service/toast-service';
import { Observable } from 'rxjs/Observable';
import { Environments } from './../../environments/environments';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from "rxjs/operators";
import { Storage } from "@ionic/storage";
import 'rxjs/add/observable/fromPromise';
import { of } from "rxjs/observable/of";
import { User } from '../../models/user';
import { BehaviorSubject } from "rxjs/BehaviorSubject";

@Injectable()
export class AuthServiceProvider {

  public userLogged: User;
  public isLogged$: BehaviorSubject<boolean>;

  constructor(
    public http: HttpClient,
    private storage: Storage,
    private toastService: ToastServiceProvider
  ) {
    this.isLogged$ = new BehaviorSubject<boolean>(false);
  }

  public async login(email: string, password: string): Promise<any> {
    try {
      const res = await this.http.post<any>(Environments.ENDPOINTS.LOGIN, { email, password }).toPromise();
      if (res) {
        localStorage.setItem(Environments.STORAGE_KEYS.TOKEN, res.token)
        const userStorage = res;
        delete userStorage.token;
        localStorage.setItem(Environments.STORAGE_KEYS.LOGGED_USER, JSON.stringify(res.user));
        this.isLogged$.next(true);
        return true;
      }
    } catch (error) {
      switch (error.status) {
        case 401:
          this.toastService.showToast('INVALID_EMAIL_OR_PASSWORD');
          break;

        default:
          this.toastService.showToast('CAN_NOT_CONNECT_TO_SERVER');
          break;
      }

      return Promise.reject(error);
    }
  }

  public async getLoggedUser() {
    try {
      const res = await this.http.get<any>(Environments.ENDPOINTS.LOGIN)
        .pipe(
          tap(() => {
            this.isLogged$.next(true);
          })
        )
        .toPromise();
      if (res) {
        localStorage.setItem(Environments.STORAGE_KEYS.LOGGED_USER, res);
        return res;
      }
      return localStorage.getItem(Environments.STORAGE_KEYS.LOGGED_USER);
    } catch (error) {
      return localStorage.getItem(Environments.STORAGE_KEYS.LOGGED_USER);
    }
  }

  public logout() {
    localStorage.removeItem(Environments.STORAGE_KEYS.TOKEN);
  }

  public getToken(): Observable<string> {
    return of(localStorage.getItem(Environments.STORAGE_KEYS.TOKEN));
  }

  public recoverPassword(email: string): Promise<any> {
    return this.http.post<any>(Environments.ENDPOINTS.RECOVER_PASSWORD, { email }).toPromise();
  }

}
