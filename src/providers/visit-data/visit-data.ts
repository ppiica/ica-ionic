import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {DataBaseProvider} from "../data-base/data-base";
import {Visit} from "../../models/visitModel";
import {Queries} from "../../environments/sqlQueries";
import {SQLiteObject} from "@ionic-native/sqlite";

@Injectable()
export class VisitDataProvider {

  private db: SQLiteObject;

  constructor(
    public dataBaseService: DataBaseProvider,
  ) {
    this.getDB();
  }

  public async saveVisitData(visitData) {
    console.log("SAve", visitData);
    try {
      if (visitData) {
        await this.getDB();
        const queryParams = [visitData.id, JSON.stringify(visitData)];
        await this.db.executeSql(Queries.VISIT_DATA_QUERY.INSERT_VISIT_DATA, queryParams)
      }
    } catch (e) {
      console.log(e);
    }
  }

  public async getVisitData(id: number): Promise<any> {
    if (id) {
      try {
        await this.getDB();
        console.log(Queries.VISIT_DATA_QUERY.SELECT_VISIT_DATA, [id]);

        const rs = await this.db.executeSql(Queries.VISIT_DATA_QUERY.SELECT_VISIT_DATA, [id]);
        console.log(rs);
        if (rs && rs.rows.length) {
          console.log("Ha ha",rs.rows.item(0));
          return JSON.parse(rs.rows.item(0).json);
        }
      } catch (e) {
        return Promise.reject(e);
      }
    }
  }

  private async getDB() {
    if (!this.db) {
      this.db = await this.dataBaseService.getDataBase();
    }
  }

}
