import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class AlertServiceProvider {

  constructor(
    private alertCtrl: AlertController,
    private translate: TranslateService
  ) {

  }

  public showAlert(title: string, content: string) {
    this.showAlertWithButtons(title, content, true);
  }

  public showAlertWithButtons(title: string, content: string, noShowButtons?: boolean): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.translate.get([title, content, 'OK', 'CANCEL'])
        .subscribe((values) => {
          const buttons: any[] = [
            {
              text: values['OK'],
              handler: resolve
            }
          ];
          if (!noShowButtons) {
            buttons.push({
              text: values['CANCEL'],
              role: 'cancel',
              handler: reject
            });
          }
          const alertConfig = {
            title: values[title],
            message: values[content],
            buttons
          };
          const alert = this.alertCtrl.create(alertConfig);
          alert.present();
        }).unsubscribe();
    });

  }

}
