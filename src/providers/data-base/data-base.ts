import { Queries } from './../../environments/sqlQueries';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class DataBaseProvider {

  private icaLocalDB: SQLiteObject;
  public bsIcaLocalDB: BehaviorSubject<SQLiteObject>;

  constructor(
    private sqlLite: SQLite,
  ) {
    this.bsIcaLocalDB = new BehaviorSubject<SQLiteObject>(null);
  }

  public async initDB() {
    try {
      this.icaLocalDB = await this.sqlLite.create({
        name: 'ica.db',
        location: 'default',
      });
      this.bsIcaLocalDB.next(this.icaLocalDB);
      this.initTables();
    } catch (error) {
      console.error(error);
    }
  }

  /**
   * getDataBase
   */
  public async getDataBase(): Promise<SQLiteObject> {
    if (this.icaLocalDB) {
      console.log("here");

      return this.icaLocalDB;
    } else {
      console.log("here 2");

      return await this.waitTheDB();
    }
  }

  private waitTheDB(): Promise<SQLiteObject> {
    return new Promise((resolve, reject) => {
      this.bsIcaLocalDB
        .subscribe(resolve, reject).
        unsubscribe();
    });
  }

  private initTables() {
    this.icaLocalDB.executeSql(Queries.VISITS_QUERIES.CREATE_TABLE, []);
    this.icaLocalDB.executeSql(Queries.CATEGORY_QUERIES.CREATE_TABLE, []);
    this.icaLocalDB.executeSql(Queries.VISIT_DATA_QUERY.CREATE_TABLE, []);
    this.icaLocalDB.executeSql(Queries.SPECIES_QUERIES.CREATE_TABLE, []);
  }

}
