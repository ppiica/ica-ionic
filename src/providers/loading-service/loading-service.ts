import { Injectable } from '@angular/core';
import { LoadingController, Loading } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class LoadingServiceProvider {

  constructor(
    private loadingCtrl: LoadingController,
    private translateService: TranslateService,
  ) {
  }

  public getLoading(content: string): Promise<Loading> {
    return this.createLoading(content);
  }

  private createLoading(content: string): Promise<Loading> {
    return new Promise((resolve, reject) => {
      this.translateService.get([content])
        .subscribe(values => {
          const loading: Loading = this.loadingCtrl.create({
            content: values[content],
          });
          resolve(loading);
        }, reject)
        .unsubscribe();
    });
  }

}
