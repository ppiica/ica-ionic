import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {DataBaseProvider} from "../data-base/data-base";
import {NetworkServiceProvider} from "../network-service/network-service";
import {SQLiteObject} from "@ionic-native/sqlite";
import {Queries} from "../../environments/sqlQueries";
import {Category} from "../../models/evaluation";
import {Environments} from "../../environments/environments";
import {Species} from "../../models/species";

@Injectable()
export class SpeciesProvider {

  private db: SQLiteObject;
  private online: boolean;

  constructor(
    public http: HttpClient,
    public dataBaseService: DataBaseProvider,
    private networkService: NetworkServiceProvider
  ) {
    this.getDB();
    this.networkService.networkBhs.subscribe((status) => this.online = status);
  }

  public getSpecies(): Promise<Species[]> {
    return new Promise((resolve, reject) => {
      if (this.online) {
        this.getSpeciesBackend()
          .then((species) => {
            resolve(species);
            this.insertSpeciesArray(species);
          })
          .catch((error) => {
            console.log(error);
            this.getSpeciesFromLocal()
              .then(resolve);
          });
      } else {
        this.getSpeciesFromLocal()
          .then(resolve);
      }
    });
  }

  private getSpeciesBackend(): Promise<Species[]> {
    return this.http.get<Category[]>(Environments.ENDPOINTS.SPECIES).toPromise();
  }

  private async getSpeciesFromLocal(): Promise<Species[]> {
    try {
      await this.getDB();
      const rs = await this.db.executeSql(Queries.SPECIES_QUERIES.SELECT_SPECIES, []);
      if (rs && rs.rows.length) {
        const Species: Species[] = [];
        for (let i = 0; i < rs.rows.length; i++) {
          const speciesObject = JSON.parse(rs.rows.item(i).json);
          Species.push(speciesObject);
        }
        return Species;
      } else {
        return [];
      }
    } catch (err) {
      return Promise.reject(err);
    }
  }

  private insertSpeciesArray(species: Species[]) {
    if (species && species.length) {
      for (let i = 0; i < species.length; i++) {
        const speciesObject = species[i];
        this.insertSpecies(speciesObject);
      }
    }
  }

  private async insertSpecies(speciesObject: Species) {
    if (speciesObject) {
      try {
        await this.getDB();
        const queryParams = [speciesObject.id, JSON.stringify(speciesObject)];
        await this.db.executeSql(Queries.SPECIES_QUERIES.INSERT_SPECIES, queryParams)
      } catch (e) {
        console.error(e);
      }
    }
  }

  private async getDB() {
    this.db = await this.dataBaseService.getDataBase();
  }

}
