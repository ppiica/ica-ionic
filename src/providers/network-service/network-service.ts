import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Network } from '@ionic-native/network';
import { Injectable } from '@angular/core';
import { Environments } from '../../environments/environments';
import { timeout } from "rxjs/operators";

@Injectable()
export class NetworkServiceProvider {

  public networkBhs: BehaviorSubject<boolean>;

  constructor(
    private network: Network,
    private http: HttpClient
  ) {
    this.networkBhs = new BehaviorSubject(this.network.type !== 'none');
    this.polling();
    this.checkConnection();
  }

  private async checkBackendConnection(): Promise<any> {
    console.log("ping");

    try {

      const ping = await this.http.get(Environments.ENDPOINTS.STATUS)
        .pipe(
          timeout(10000)
        )
        .toPromise();
      if (ping) {
        this.networkBhs.next(true);
      } else {
        this.networkBhs.next(false);
      }
    } catch (error) {
      console.error(error);
      this.networkBhs.next(false);
    }
  }

  private checkConnection() {
    this.checkBackendConnection();
    this.network.onConnect().subscribe(() => {
      this.checkBackendConnection();
    });
    this.network.onDisconnect().subscribe(() => this.networkBhs.next(false));;
  }

  private polling() {
    setInterval(() => {
      this.checkBackendConnection();
    }, 60000);
  }

}
