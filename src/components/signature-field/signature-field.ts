import { LoadingServiceProvider } from './../../providers/loading-service/loading-service';
import { Environments } from './../../environments/environments';
import { Component, Input, Output, EventEmitter, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, NG_VALIDATORS, ControlValueAccessor, Validator, AbstractControl } from '@angular/forms';
import { Modal, ModalController } from 'ionic-angular';

@Component({
  selector: 'signature-field',
  templateUrl: 'signature-field.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SignatureFieldComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => SignatureFieldComponent),
      multi: true,
    }
  ]
})
export class SignatureFieldComponent implements ControlValueAccessor, Validator {

  @Input()
  public text: string;

  @Input()
  public signature: string;

  @Output()
  public submitSignature: EventEmitter<string>;

  public onChange: (...any) => any;

  public onTouch: (...any) => any;

  public disabled: boolean;


  constructor(
    private modalCtrl: ModalController,
    private loadingService: LoadingServiceProvider
  ) {
    this.submitSignature = new EventEmitter();
  }

  public openSignatureModal() {
    const signatureModal: Modal = this.modalCtrl.create('SignaturePage', { title: this.text });
    signatureModal.present();
    signatureModal.onDidDismiss((data) => {
      if (data && data.signature) {

            this.signature = data.signature;
            this.notifyChanges();
            /*this.saveSignature(data.signature)
              .then((filePath) => {
                this.filename = filePath + '?caching=' + new Date().getTime();
                this.notifyChanges();
                loading.dismiss();
              })
              .catch(err => console.error(err));*/
      }
    });
  }

  private notifyChanges() {
    if (this.onChange) {
      this.onChange(this.signature);
    }
    this.submitSignature.emit(this.signature);
  }

  // Implements methods

  public writeValue(signature): void {
    if (signature) {
      this.signature = signature;
      this.notifyChanges();
    }
  }

  public registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  public setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  public validate(c: AbstractControl): { [key: string]: any; } {
    console.log(c)
    if (c.value) {
      return (c.value) ? null : {
        required: false
      }
    }
    return null;
  }

  /*private saveSignature(signature: string): Promise<string> {
    return new Promise((resolve, reject) => {
      this.fileManager.writeFile(Environments.SIGNATURES_FOLDER, `${this.text}_${new Date().valueOf()}.png`, signature)
        .then((res) => {
          resolve(res.nativeURL)
        })
        .catch(err => {
          reject(err);
        });
    });
  }*/

}
