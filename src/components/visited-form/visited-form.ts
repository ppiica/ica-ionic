import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidatorFn } from '@angular/forms';

@Component({
  selector: 'visited-form',
  templateUrl: 'visited-form.html'
})
export class VisitedFormComponent implements OnInit {

  public visitedForm: FormGroup;
  private idNumberValidator: ValidatorFn;
  private onlyLettersValidator: ValidatorFn;
  private requiredMaxLengthValidator: ValidatorFn;

  @Output()
  public onVisitedChange: EventEmitter<any>;

  @Output()
  public isVisitedValid: EventEmitter<any>;

  constructor(
    private formBuilder: FormBuilder
  ) {
    this.onVisitedChange = new EventEmitter();
    this.isVisitedValid = new EventEmitter<any>();
    this.initValidators();
    this.initForm();

  }

  @Input()
  set visited(visited) {
    if (visited && visited.personWhoAttendsId) {
      const formValue = {
        personWhoAttendsId: visited.personWhoAttendsId,
        personWhoAttendsName: visited.personWhoAttendsName || '',
        personWhoAttendsPhone: visited.personWhoAttendsPhone || ''
      }
      this.visitedForm.setValue(visited);
    }
  }

  ngOnInit(): void {
    console.log(this.visited);

  }

  private initForm() {
    this.visitedForm = this.formBuilder.group({
      personWhoAttendsId: ['', this.idNumberValidator],
      personWhoAttendsName: ['', this.onlyLettersValidator],
      personWhoAttendsPhone: ['']
    });
    this.visitedForm.valueChanges.subscribe((values) => {
      console.log(values);
      this.onVisitedChange.next(values);
      this.isVisitedValid.emit(this.visitedForm.valid);
    });

  }

  private initValidators() {
    this.requiredMaxLengthValidator = Validators.compose([Validators.minLength(2), Validators.maxLength(50), Validators.required]);
    this.idNumberValidator = Validators.compose([Validators.pattern('^[1-9][0-9]*$'), Validators.required]);
    this.onlyLettersValidator = Validators.compose([Validators.pattern('^[a-zA-Z áéíóúÁÉÍÓÚñÑ]*$'), , Validators.required]);
  }

}
