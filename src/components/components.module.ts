import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { SignatureFieldComponent } from './signature-field/signature-field';
import { IonicModule } from 'ionic-angular';
import { RequerimentFieldComponent } from './requeriment-field/requeriment-field';
import { VisitedFormComponent } from './visited-form/visited-form';

@NgModule({
	declarations: [SignatureFieldComponent,
    RequerimentFieldComponent,
    VisitedFormComponent],
	imports: [
    TranslateModule.forChild(),
    IonicModule
  ],
	exports: [SignatureFieldComponent,
    RequerimentFieldComponent,
    VisitedFormComponent]
})
export class ComponentsModule {}
