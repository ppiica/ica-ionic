import { AuthServiceProvider } from './../providers/auth-service/auth-service';
import { Component, ViewChild } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateService } from '@ngx-translate/core';
import { DataBaseProvider } from '../providers/data-base/data-base';
import { Nav } from 'ionic-angular';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  public rootPage: any = '';
  public isLogged: boolean;

  @ViewChild(Nav) nav: Nav;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    translateService: TranslateService,
    dataBase: DataBaseProvider,
    private authService: AuthServiceProvider
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.setRootPage();
      dataBase.initDB();
      statusBar.styleDefault();
      splashScreen.hide();
      translateService.setDefaultLang('es');
      translateService.use('es');
      this.authService.isLogged$.subscribe((isLogged) => {
        if (isLogged) {
          this.isLogged = isLogged;
        }
      });
    });
  }

  public goToHome() {
    this.nav.setRoot('HomePage');
  }
  public goToVisits() {
    this.nav.setRoot('VisitsListPage');
  }
  public goToLogin() {
    this.isLogged = false;
    this.authService.logout();
    this.nav.setRoot('LoginPage');
  }

  public goToHelp() {
    this.nav.setRoot('HelpPage');
  }

  public goToAbout() {
    this.nav.setRoot('AboutPage');
  }

  private setRootPage() {
    this.authService.getToken()
      .subscribe((token) => {
        console.log(token);
        if (token) {
          this.rootPage = 'VisitsListPage';
          this.isLogged = true;
        } else {
          this.rootPage = 'LoginPage';
        }
      }, () => {
        this.rootPage = 'LoginPage';
      });
  }

}

