import { AppSettings } from './app.settings';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, LOCALE_ID } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SQLite } from "@ionic-native/sqlite";
import { Camera } from '@ionic-native/camera';
import { SignaturePadModule } from "angular2-signaturepad";
import { IonicStorageModule, Storage } from "@ionic/storage";
import es from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';
import { Network } from "@ionic-native/network";

import { MyApp } from './app.component';
import { DataBaseProvider } from '../providers/data-base/data-base';
import { RegionsProvider } from '../providers/regions/regions';
import { RequirementDetail } from '../pages/requirement/requirementDetail';
import { LoadingServiceProvider } from '../providers/loading-service/loading-service';
import { VisitService } from '../providers/visit-service/visit-service';
import { EvaluationsServiceProvider } from '../providers/evaluations-service/evaluations-service';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { AuthInterceptor } from '../interceptors/auth-interceptor';
import { ToastServiceProvider } from '../providers/toast-service/toast-service';
import { NetworkServiceProvider } from '../providers/network-service/network-service';
import { VisitDataProvider } from '../providers/visit-data/visit-data';
import { SpeciesProvider } from '../providers/species/species';
import { AlertServiceProvider } from '../providers/alert-service/alert-service';


// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

registerLocaleData(es);


@NgModule({
  declarations: [
    MyApp,
    RequirementDetail
  ],
  imports: [
    BrowserModule,
    SignaturePadModule,
    IonicModule.forRoot(MyApp, {
      monthNames: AppSettings.monthNames,
      monthShortNames: AppSettings.monthShortNames,
      dayNames: AppSettings.dayNames,
      dayShortNames: AppSettings.dayShortNames,
    }),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    RequirementDetail
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    { provide: LOCALE_ID, useValue: 'es' },
    DataBaseProvider,
    SQLite,
    RegionsProvider,
    Camera,
    VisitService,
    EvaluationsServiceProvider,
    AuthServiceProvider,
    ToastServiceProvider,
    Network,
    NetworkServiceProvider,
    VisitDataProvider,
    SpeciesProvider,
    LoadingServiceProvider,
    AlertServiceProvider
  ]
})
export class AppModule { }
