export interface Owner {
  id: number;
  firstName: string;
  lastNameOne: string;
  lastNameTwo: string;
  phone: number;
  email: string;
  address: string;
  createdAt: Date;
  updatedAt: Date;
  isRemove: boolean;
}

interface Sidewalk {
  name: string;
  municipality: {
    name: string;
    department: {
      name: string
    }
  }
}

export interface Farm {
  registerNumber: number;
  name: string;
  latitude: number;
  longitude: number;
  area: number;
  sidewalkId: number;
  ownerId: number;
  officeId: string;
  createdAt: Date;
  updatedAt: Date;
  isRemove: boolean;
  owner: Owner;
  sidewalk: Sidewalk;
}

export interface Technical {
  id: number;
  firstName: string;
  lastNameOne: string;
  lastNameTwo?: any;
  createdAt: Date;
  updatedAt: Date;
  fullName: string;
  professionalRegistration: string;
  coordinatorId: number;
}

export interface Status {
  id: number;
  description: string;
  createdAt: Date;
  updatedAt: Date;
}

export interface Visit {
  id: number;
  date: Date;
  nextVisitDate?: any;
  observations?: any;
  gradualCompliancePlan?: any;
  ownerFarmSign?: any;
  inspectorSign?: any;
  farmRegisterNumber: number;
  statusId: number;
  technicalId: string;
  createdAt: Date;
  updatedAt: Date;
  isRemove: boolean;
  personWhoAttendsId?: any;
  personWhoAttendsName?: any;
  personWhoAttendsPhone?: any;
  farm: Farm;
  technical: Technical;
  status: Status;
  qualifications: any[];
  productiveActivities: any[];
}
