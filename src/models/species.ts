export interface Species {
  id: number;
  description: string;
}
