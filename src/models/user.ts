export class User {

  public id?: string;
  public email?: string;
  public createdAt?: Date;
  public updatedAt?: Date;
  public roleId?: number;
  public techniciansIds?: Array<string>;
  public userType?: { name: string };
  public profile: {
    firstName: string;
    lastNameOne?: string;
    lastNameTwo?: string;
    phone?: number;
    officeId?: string;
  };
  public password?: string;
}
