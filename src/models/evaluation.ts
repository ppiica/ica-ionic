export interface Item {
  id: number;
  description: string;
  categoryId: number;
  createdAt: Date;
  updatedAt: Date;
}

export interface Category {
  id: number;
  description: string;
  maxScore: number;
  createdAt: Date;
  updatedAt: Date;
  items: Item[];
  categoryPercent: number;
}
