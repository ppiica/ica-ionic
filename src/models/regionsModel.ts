export interface IDepartment {
  id: number;
  nombre: string;
}

export interface IMunicipality {
  id: number;
  nombre: string;
  idDepartamento: number;
}

export interface ISideWalk {
  id: number;
  nombre: string;
  idMunicipio: number;
}
