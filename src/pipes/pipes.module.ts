import { NgModule } from '@angular/core';
import { TotalCriteriaPipe } from './total-criteria/total-criteria';
import { PercentTotalCriteriaPipe } from './percent-total-criteria/percent-total-criteria';
import { ProductiveActivityInventoryPipe } from './productive-activity-inventory/productive-activity-inventory';
import { PercentPerCategoryPipe } from './percent-per-category/percent-per-category';
@NgModule({
  declarations: [TotalCriteriaPipe,
    PercentTotalCriteriaPipe,
    ProductiveActivityInventoryPipe,
    PercentPerCategoryPipe],
  imports: [],
  exports: [TotalCriteriaPipe,
    PercentTotalCriteriaPipe,
    ProductiveActivityInventoryPipe,
    PercentPerCategoryPipe]
})
export class PipesModule { }
