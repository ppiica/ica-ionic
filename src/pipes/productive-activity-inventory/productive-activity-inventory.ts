import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the ProductiveActivityInventoryPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'productiveActivityInventory',
})
export class ProductiveActivityInventoryPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(productiveActivity: any) {
    if (productiveActivity) {
      let total = 0;
      if (productiveActivity.breed) {
        total += productiveActivity.breed;
      }
      if (productiveActivity.lift) {
        total += productiveActivity.lift;
      }
      if (productiveActivity.fatten) {
        total += productiveActivity.fatten;
      }
      if (productiveActivity.fullCycle) {
        total += productiveActivity.fullCycle;
      }
      return total;
    }
  }
}
