import { Category } from './../../models/evaluation';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'percentPerCategory',
})
export class PercentPerCategoryPipe implements PipeTransform {

  transform(category: Category, qualifications: any[]) {
    if (category && qualifications) {
      let qualificationsByCategory = 0;
      let qualificationsValue = 0;

      for (let i = 0; i < category.items.length; i++) {
        const item = category.items[i];
        const qualification = qualifications[item.id];
        if (qualification) {
          qualificationsValue += qualification.score;
          qualificationsByCategory++;
        }
      }

      const valueMax = qualificationsByCategory * category.maxScore;
      return qualificationsValue / valueMax;
    }
  }

}
