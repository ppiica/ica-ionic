import { Category } from './../../models/evaluation';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'totalCriteria',
})
export class TotalCriteriaPipe implements PipeTransform {

  transform(category: Category, qualifications: any[]) {
    if (category && qualifications) {
      let qualificationsByCategory = 0;

      for (let i = 0; i < category.items.length; i++) {
        const item = category.items[i];
        const qualification = qualifications[item.id];
        if (qualification) {
          qualificationsByCategory++;
        }
      }
      return qualificationsByCategory;
    }
  }

}
