import { Category } from './../../models/evaluation';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'percentTotalCriteria',
})
export class PercentTotalCriteriaPipe implements PipeTransform {

  transform(categories: Category[], qualifications: any[]) {
    if (categories && qualifications) {
      let percent = 0;
      let totalReference = 0;

      for (let i = 0; i < categories.length; i++) {
        const category = categories[i];
        let percentPerCategory = 0;
        if (category && qualifications) {
          let qualificationsByCategory = 0;
          let qualificationsValue = 0;

          for (let i = 0; i < category.items.length; i++) {
            const item = category.items[i];
            const qualification = qualifications[item.id];
            if (qualification) {
              qualificationsValue += qualification.score;
              qualificationsByCategory++;
            }
          }

          const valueMax = qualificationsByCategory * category.maxScore;
          if (valueMax > 0) {
            totalReference += category.categoryPercent;
            percentPerCategory = qualificationsValue / valueMax;
            percentPerCategory = percentPerCategory * category.categoryPercent;
            percent = percent + percentPerCategory;
          }

        }

      }
      return percent / totalReference;
    }
  }
}
