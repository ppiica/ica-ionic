// const API_ROOT = 'http://192.168.1.55:3000/api/v1';
const API_ROOT = 'http://34.217.75.3:3000/api/v1';


export class Environments {

  static readonly ENDPOINTS = {
    LOGIN: API_ROOT + '/auth/login',
    LOGGED_USER: API_ROOT + '/auth/loggedUser',
    VISITS: API_ROOT + '/technicals/{{id}}/visits',
    CATEGORIES: API_ROOT + '/categories',
    RECOVER_PASSWORD: API_ROOT + '/auth/recoverPassword',
    STATUS: API_ROOT + '/utils/ping',
    FINISH_VISITS: API_ROOT + '/visits/{{id}}/finish',
    SPECIES: API_ROOT + '/species'
  }

  static SIGNATURES_FOLDER = 'signatures/';

  static STORAGE_KEYS = {
    LOGGED_USER: 'LOGGED_USER',
    TOKEN: 'TOKEN'
  }

}
